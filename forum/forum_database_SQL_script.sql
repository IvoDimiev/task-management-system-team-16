create or replace table categories
(
    category_id   int auto_increment
        primary key,
    category_name varchar(40) not null,
    constraint categories_category_id_uindex
        unique (category_id),
    constraint categories_category_name_uindex
        unique (category_name)
);

create or replace table roles
(
    role_id   int auto_increment
        primary key,
    role_name varchar(10) not null,
    constraint roles_role_id_uindex
        unique (role_id)
);

create or replace table tags
(
    tag_id   int auto_increment
        primary key,
    tag_name varchar(40) not null,
    constraint tags_tag_id_uindex
        unique (tag_id),
    constraint tags_tag_name_uindex
        unique (tag_name)
);

create or replace table users
(
    user_id       int auto_increment
        primary key,
    first_name    varchar(32)          not null,
    last_name     varchar(32)          not null,
    email         varchar(100)         not null,
    username      varchar(32)          not null,
    password      varchar(100)         not null,
    creation_datr datetime             not null,
    blocked       tinyint(1) default 0 not null,
    constraint user_user_id_uindex
        unique (user_id),
    constraint users_email_uindex
        unique (email),
    constraint users_username_uindex
        unique (username)
);

create or replace table phone_numbers
(
    user_id      int     not null
        primary key,
    phone_number int(10) not null,
    constraint phone_numbers_phone_number_uindex
        unique (phone_number),
    constraint phone_numbers_user_id_uindex
        unique (user_id),
    constraint phone_numbers_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create or replace table posts
(
    post_id       int auto_increment
        primary key,
    user_id       int           not null,
    title         varchar(64)   not null,
    content       varchar(8192) not null,
    votes         int           not null,
    creation_datr datetime      not null,
    constraint posts_post_id_uindex
        unique (post_id),
    constraint posts_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create or replace table category_post
(
    category_id int not null,
    post_id     int not null,
    constraint category_post_post_id_uindex
        unique (post_id),
    constraint category_post_categories_category_id_fk
        foreign key (category_id) references categories (category_id),
    constraint category_post_posts_post_id_fk
        foreign key (post_id) references posts (post_id)
);

create or replace table comments
(
    comment_id    int auto_increment
        primary key,
    post_id       int           not null,
    votes         int           not null,
    content       varchar(8192) not null,
    parent_id     int           null,
    creation_date datetime      not null,
    constraint comments_comment_id_uindex
        unique (comment_id),
    constraint comments_posts_post_id_fk
        foreign key (post_id) references posts (post_id)
);

create or replace table comment_votes
(
    comment_id int not null,
    user_id    int not null,
    constraint comment_votes_comments_comment_id_fk
        foreign key (comment_id) references comments (comment_id),
    constraint comment_votes_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create or replace table post_votes
(
    post_id int not null,
    user_id int not null,
    constraint post_votes_posts_post_id_fk
        foreign key (post_id) references posts (post_id),
    constraint post_votes_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create or replace table tag_post
(
    tag_id  int not null,
    post_id int not null,
    constraint tag_post_posts_post_id_fk
        foreign key (post_id) references posts (post_id),
    constraint tag_post_tags_tag_id_fk
        foreign key (tag_id) references tags (tag_id)
);

create or replace table user_roles
(
    user_id int not null,
    role_id int not null,
    constraint user_roles_roles_role_id_fk
        foreign key (role_id) references roles (role_id),
    constraint user_roles_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

