package operations.userOperations;

import core.Contracts.TaskManagementRepository;
import models.contracts.User;
import operations.BaseOperation;
import utils.ValidationHelpers;

import java.util.List;

public class ChangePasswordOperation extends BaseOperation {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    private final static String PASSWORD_CHANGED = "User %s changed password successfully!";
    private final static String WRONG_OLD_PASSWORD = "Incorrect old password.";

    public ChangePasswordOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        String oldPassword = parameters.get(0);
        String newPassword = parameters.get(1);

        return changePassword(oldPassword, newPassword);
    }

    private String changePassword(String oldPassword, String newPassword) {
        User loggedUser = getTaskManagementRepository().getLoggedInUser();
        if (!loggedUser.getPassword().equalsIgnoreCase(oldPassword)) {
            return WRONG_OLD_PASSWORD;
        }
        loggedUser.changePassword(newPassword);

        return String.format(PASSWORD_CHANGED, loggedUser.getUsername());
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }
}
