package operations.userOperations;

import core.Contracts.TaskManagementRepository;
import models.contracts.User;
import operations.BaseOperation;
import utils.ValidationHelpers;

import java.util.List;

public class LogInOperation extends BaseOperation {

    public static final int ARGUMENTS_COUNT = 2;
    private final static String USER_LOGGED_IN = "User %s successfully logged in!";
    private final static String WRONG_USERNAME_OR_PASSWORD = "Wrong username or password!";
    public final static String USER_LOGGED_IN_ALREADY = "User %s is logged in! Please log out first!";

    public LogInOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        throwIfUserLoggedIn();
        ValidationHelpers.validateArgumentsCount(parameters, ARGUMENTS_COUNT);
        String username = parameters.get(0);
        String password = parameters.get(1);

        return logIn(username, password);
    }

    @Override
    protected boolean requiresLogin() {
        return false;
    }

    private String logIn(String username, String password) {
        User userFound = getTaskManagementRepository().findUserByUsername(username);
        if (!userFound.getPassword().equals(password)) {
            throw new IllegalArgumentException(WRONG_USERNAME_OR_PASSWORD);
        }
        getTaskManagementRepository().login(userFound);

        return String.format(USER_LOGGED_IN, username);
    }

    private void throwIfUserLoggedIn() {
        if (getTaskManagementRepository().hasLoggedInUser()) {
            throw new IllegalArgumentException(
                    String.format(USER_LOGGED_IN_ALREADY, getTaskManagementRepository().getLoggedInUser().getUsername())
            );
        }
    }
}
