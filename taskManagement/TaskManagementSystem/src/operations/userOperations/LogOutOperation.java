package operations.userOperations;

import core.Contracts.TaskManagementRepository;
import operations.BaseOperation;
import utils.ValidationHelpers;

import java.util.List;

public class LogOutOperation extends BaseOperation {
    public final static String USER_LOGGED_OUT = "You logged out!";
    public static final int EXPECTED_NUMBER_OF_PARAMETERS = 0;

    public LogOutOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        getTaskManagementRepository().logout();

        return USER_LOGGED_OUT;
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }
}
