package operations.userOperations;

import core.Contracts.TaskManagementRepository;
import models.contracts.Team;
import models.contracts.User;
import models.tasks.enums.UserRole;
import operations.BaseOperation;
import utils.ValidationHelpers;

import java.util.List;


public class InviteUserOperation extends BaseOperation {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;
    private final static String USER_REGISTERED = "User %s invited to team %s!";
    private final static String USER_INVITES_TO_TEAM = "User: '%s' Invited '%s' to team %s";
    private final static String USER_INVITED_TO_TEAM = "User '%s' was invited to team %s by %s";
    private final CreateNewUserOperation createNewUserCommand;

    public InviteUserOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
        createNewUserCommand = new CreateNewUserOperation(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        Team team = getTaskManagementRepository().findTeamByName(parameters.get(0));
        List<String> updatedParameters = List.of(parameters.get(1), parameters.get(2), UserRole.DEVELOPER.toString());
        User invitedUser = createNewUserCommand.executeOperation(updatedParameters);
        User loggedUser = getTaskManagementRepository().getLoggedInUser();
        invitedUser.createLog(String.format(USER_INVITED_TO_TEAM, invitedUser.getUsername(),
                team.getName(), loggedUser.getUsername()));
        loggedUser.createLog(String.format(USER_INVITES_TO_TEAM, loggedUser.getUsername(), invitedUser.getUsername(), team.getName()));
        team.addUser(invitedUser);

        return String.format(USER_REGISTERED, invitedUser.getUsername(), team.getName());
    }


    @Override
    protected boolean requiresLogin() {
        return true;
    }
}
