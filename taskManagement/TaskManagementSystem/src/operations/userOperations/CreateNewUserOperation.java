package operations.userOperations;

import core.Contracts.TaskManagementRepository;
import models.contracts.User;
import models.tasks.enums.UserRole;
import utils.ParsingHelpers;
import utils.ValidationHelpers;

import java.util.List;

public class CreateNewUserOperation {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;
    private final static String USER_ALREADY_EXIST = "User %s already exist. Choose a different username!";
    private final TaskManagementRepository taskManagementRepository;


    public CreateNewUserOperation(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    public User executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        String username = parameters.get(0);
        String password = parameters.get(1);
        UserRole role = ParsingHelpers.tryParseEnum(parameters.get(2), UserRole.class);

        return registerUser(username, password, role);
    }

    private User registerUser(String username, String password, UserRole role) {
        throwIfUsernameAlreadyExists(username);
        User user = taskManagementRepository.createUser(username, password, role);
        taskManagementRepository.addUser(user);

        return user;
    }

    private void throwIfUsernameAlreadyExists(String username) {
        if (taskManagementRepository.getUsers().stream().anyMatch(u -> u.getUsername().equalsIgnoreCase(username))) {
            throw new IllegalArgumentException(String.format(USER_ALREADY_EXIST, username));
        }
    }

}
