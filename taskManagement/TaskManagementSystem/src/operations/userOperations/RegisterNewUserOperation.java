package operations.userOperations;

import core.Contracts.TaskManagementRepository;
import models.contracts.User;
import models.tasks.enums.UserRole;
import operations.BaseOperation;
import utils.ValidationHelpers;

import java.util.List;

public class RegisterNewUserOperation extends BaseOperation {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    private final static String USER_REGISTERED = "User %s registered successfully!";
    public final static String USER_LOGGED_IN_ALREADY = "User %s is logged in! Please log out first!";
    private final CreateNewUserOperation createNewUserCommand;

    public RegisterNewUserOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
        createNewUserCommand = new CreateNewUserOperation(taskManagementRepository);
    }

    @Override
    public String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        throwIfLoggedInAlready();
        parameters.add(UserRole.ADMIN.name());
        User user = createNewUserCommand.executeOperation(parameters);
        getTaskManagementRepository().login(user);

        return String.format(USER_REGISTERED, user.getUsername());
    }

    private void throwIfLoggedInAlready() {
        if (getTaskManagementRepository().hasLoggedInUser()) {
            throw new IllegalArgumentException(String.format(USER_LOGGED_IN_ALREADY, getTaskManagementRepository().getLoggedInUser().getUsername()));
        }
    }

    @Override
    protected boolean requiresLogin() {
        return false;
    }
}
