package operations.listingOperations;

import core.Contracts.TaskManagementRepository;
import exceptions.ElementNotFoundException;
import models.contracts.Board;
import models.tasks.contracts.Task;
import operations.BaseOperation;
import utils.ListingHelpers;
import utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

public class ListAllTasksOperation extends BaseOperation {
    public static final int EXPECTED_NUMBER_OF_PARAMETERS = 2;
    public static final String NO_REGISTERED_TASKS = "There are no registered tasks.";

    private final List<Task> tasks;

    public ListAllTasksOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
        tasks = new ArrayList<>();
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        List<String> teamAndBoardNameParameters = parameters.subList(0, 2);
        Board board = getTaskManagementRepository().getCurrentBoard(teamAndBoardNameParameters);
        List<Task> tasks = board.getTasks();
        if (tasks.isEmpty()) {
            throw new ElementNotFoundException(NO_REGISTERED_TASKS);
        }

        return ListingHelpers.tasksToString(tasks);
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }
}
