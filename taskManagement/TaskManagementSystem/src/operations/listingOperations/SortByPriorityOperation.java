package operations.listingOperations;

import core.Contracts.TaskManagementRepository;
import exceptions.ElementNotFoundException;
import models.contracts.Board;
import models.tasks.contracts.Prioritisable;
import operations.BaseOperation;
import utils.ListingHelpers;
import utils.ValidationHelpers;

import java.util.Comparator;
import java.util.List;

public class SortByPriorityOperation extends BaseOperation {
    public static final int EXPECTED_NUMBER_OF_PARAMETERS = 2;
    public static final String NO_REGISTERED_PRIORITISABLES = "There are no registered prioritisables.";

    public SortByPriorityOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        List<String> teamAndBoardNameParameters = parameters.subList(0, 2);
        Board board = getTaskManagementRepository().getCurrentBoard(teamAndBoardNameParameters);
        List<Prioritisable> prioritisables = board.getPrioritisable();
        if (prioritisables.isEmpty()) {
            throw new ElementNotFoundException(NO_REGISTERED_PRIORITISABLES);
        }
        compareTo(prioritisables);

        return ListingHelpers.prioritisablesToString(prioritisables);
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }

    private <T extends Prioritisable> void compareTo(List<T> tasks) {
        Comparator<T> sorted = Comparator.comparing(T::getPriority);

        tasks.sort(sorted);
    }
}
