package operations.listingOperations;

import core.Contracts.TaskManagementRepository;
import exceptions.ElementNotFoundException;
import models.contracts.Board;
import models.tasks.contracts.Assignable;
import operations.BaseOperation;
import utils.ListingHelpers;
import utils.ValidationHelpers;

import java.util.List;
import java.util.stream.Collectors;

public class FilterByAssigneeOperation extends BaseOperation {
    public static final int EXPECTED_NUMBER_OF_PARAMETERS = 3;
    public static final String NO_REGISTERED_TASKS = "There are no registered tasks.";

    public FilterByAssigneeOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        List<String> teamAndBoardNameParameters = parameters.subList(0, 2);
        Board board = getTaskManagementRepository().getCurrentBoard(teamAndBoardNameParameters);
        String assigneeUsername = parameters.get(2);
        List<Assignable> allAssignables = board.getAssignable();
        if (allAssignables.isEmpty()) {
            throw new ElementNotFoundException(NO_REGISTERED_TASKS);
        }
        List<Assignable> filteredAssignables = filterList(assigneeUsername, allAssignables);

        return ListingHelpers.assignablesToString(filteredAssignables);
    }

    private <T extends Assignable> List<T> filterList(String username, List<T> tasks) {
        return tasks.stream().filter(task -> task.getAssignee().getUsername().equalsIgnoreCase(username)).collect(Collectors.toList());
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }
}
