package operations.listingOperations;

import core.Contracts.TaskManagementRepository;
import exceptions.ElementNotFoundException;
import models.contracts.Board;
import models.tasks.contracts.Feedback;
import operations.BaseOperation;
import utils.ListingHelpers;
import utils.ValidationHelpers;

import java.util.Comparator;
import java.util.List;

public class SortFeedbackByRatingOperation extends BaseOperation {
    public static final int EXPECTED_NUMBER_OF_PARAMETERS = 2;
    public static final String NO_REGISTERED_FEEDBACKS = "There are no registered feedbacks.";

    public SortFeedbackByRatingOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        List<String> teamAndBoardNameParameters = parameters.subList(0, 2);
        Board board = getTaskManagementRepository().getCurrentBoard(teamAndBoardNameParameters);
        List<Feedback> feedbacks = board.getFeedback();
        if (feedbacks.isEmpty()) {
            throw new ElementNotFoundException(NO_REGISTERED_FEEDBACKS);
        }
        compareTo(feedbacks);

        return ListingHelpers.feedbackToString(feedbacks);
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }

    private void compareTo(List<Feedback> tasks) {
        Comparator<Feedback> sorted = Comparator.comparing(Feedback::getRating);
        tasks.sort(sorted);
    }
}
