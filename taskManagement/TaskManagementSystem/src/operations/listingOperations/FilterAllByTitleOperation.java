package operations.listingOperations;

import core.Contracts.TaskManagementRepository;
import exceptions.ElementNotFoundException;
import models.contracts.Board;
import models.tasks.contracts.Task;
import operations.BaseOperation;
import utils.ListingHelpers;
import utils.ValidationHelpers;

import java.util.List;
import java.util.stream.Collectors;

public class FilterAllByTitleOperation extends BaseOperation {
    public static final int EXPECTED_NUMBER_OF_PARAMETERS = 3;
    public  static final String NO_REGISTERED_TASKS = "There are no registered tasks.";

    public FilterAllByTitleOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        List<String> teamAndBoardNameParameters = parameters.subList(0, 2);
        Board board = getTaskManagementRepository().getCurrentBoard(teamAndBoardNameParameters);
        String filterWord = parameters.get(2);
        List<Task> allTasks = board.getTasks();
        if(allTasks.isEmpty()) {
            throw new ElementNotFoundException(NO_REGISTERED_TASKS);
        }
        List<Task> filteredTasks = allTasks.stream().filter(task -> task.getTitle().contains(filterWord)).collect(Collectors.toList());

        return ListingHelpers.tasksToString(filteredTasks);
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }
}
