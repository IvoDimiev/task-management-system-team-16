package operations.listingOperations;

import core.Contracts.TaskManagementRepository;
import exceptions.ElementNotFoundException;
import models.contracts.Board;
import models.tasks.contracts.Bug;
import models.tasks.enums.Severity;
import operations.BaseOperation;
import utils.ListingHelpers;
import utils.ParsingHelpers;
import utils.ValidationHelpers;

import java.util.List;
import java.util.stream.Collectors;

public class FilterBugBySeverityOperation extends BaseOperation {
    public static final int EXPECTED_NUMBER_OF_PARAMETERS = 3;
    public static final String NO_REGISTERED_BUGS = "There are no registered bugs.";

    public FilterBugBySeverityOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        List<String> teamAndBoardNameParameters = parameters.subList(0, 2);
        Board board = getTaskManagementRepository().getCurrentBoard(teamAndBoardNameParameters);
        Severity severity = ParsingHelpers.tryParseEnum(parameters.get(2), Severity.class);
        List<Bug> allBugs = board.getBugs();
        if(allBugs.isEmpty()) {
            throw new ElementNotFoundException(NO_REGISTERED_BUGS);
        }
        List<Bug> filteredBugs = filterList(severity, allBugs);

        return ListingHelpers.bugsToString(filteredBugs);
    }

    private List<Bug> filterList(Severity severity, List<Bug> bugs) {
        return bugs.stream().filter(task -> task.getSeverity().equals(severity)).collect(Collectors.toList());
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }
}
