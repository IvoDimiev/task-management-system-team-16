package operations.listingOperations;

import core.Contracts.TaskManagementRepository;
import exceptions.ElementNotFoundException;
import models.contracts.Board;
import models.tasks.contracts.Story;
import operations.BaseOperation;
import utils.ListingHelpers;
import utils.ValidationHelpers;

import java.util.Comparator;
import java.util.List;

public class SortStoryBySizeOperation extends BaseOperation {
    public static final int EXPECTED_NUMBER_OF_PARAMETERS = 2;
    public static final String NO_REGISTERED_STORIES = "There are no registered stories.";

    public SortStoryBySizeOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        List<String> teamAndBoardNameParameters = parameters.subList(0, 2);
        Board board = getTaskManagementRepository().getCurrentBoard(teamAndBoardNameParameters);
        List<Story> stories = board.getStories();
        if (stories.isEmpty()) {
            throw new ElementNotFoundException(NO_REGISTERED_STORIES);
        }
        compareTo(stories);

        return ListingHelpers.storiesToString(stories);
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }

    private <T extends Story> void compareTo(List<T> tasks) {
        Comparator<T> sorted = Comparator.comparing(T::getSize);
        tasks.sort(sorted);
    }
}
