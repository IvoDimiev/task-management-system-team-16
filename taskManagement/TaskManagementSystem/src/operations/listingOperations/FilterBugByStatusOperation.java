package operations.listingOperations;

import core.Contracts.TaskManagementRepository;
import exceptions.ElementNotFoundException;
import models.contracts.Board;
import models.tasks.contracts.Bug;
import models.tasks.contracts.Statusable;
import models.tasks.contracts.Task;
import models.tasks.enums.BugStatus;
import operations.BaseOperation;
import utils.ListingHelpers;
import utils.ParsingHelpers;
import utils.ValidationHelpers;

import java.util.List;
import java.util.stream.Collectors;

public class FilterBugByStatusOperation extends BaseOperation {
    public static final int EXPECTED_NUMBER_OF_PARAMETERS = 3;
    public static final String NO_REGISTERED_BUGS = "There are no registered bugs.";

    public FilterBugByStatusOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        List<String> teamAndBoardNameParameters = parameters.subList(0, 2);
        Board board = getTaskManagementRepository().getCurrentBoard(teamAndBoardNameParameters);
        Statusable bugStatus = ParsingHelpers.tryParseEnum(parameters.get(2), BugStatus.class);
        List<Bug> allBugs = board.getBugs();
        if (allBugs.isEmpty()) {
            throw new ElementNotFoundException(NO_REGISTERED_BUGS);
        }
        List<Bug> filteredBugs = filterList(bugStatus, allBugs);

        return ListingHelpers.bugsToString(filteredBugs);
    }

    private <T extends Task> List<T> filterList(Statusable status, List<T> tasks) {
        return tasks.stream().filter(task -> task.getStatus().equals(status)).collect(Collectors.toList());
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }
}
