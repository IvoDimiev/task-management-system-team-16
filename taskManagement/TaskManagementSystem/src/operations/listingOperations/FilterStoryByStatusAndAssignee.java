package operations.listingOperations;

import core.Contracts.TaskManagementRepository;
import exceptions.ElementNotFoundException;
import models.contracts.Board;
import models.tasks.contracts.Statusable;
import models.tasks.contracts.Story;
import models.tasks.enums.StoryStatus;
import operations.BaseOperation;
import utils.ListingHelpers;
import utils.ParsingHelpers;
import utils.ValidationHelpers;

import java.util.List;
import java.util.stream.Collectors;

public class FilterStoryByStatusAndAssignee extends BaseOperation {
    public static final int EXPECTED_NUMBER_OF_PARAMETERS = 4;
    public static final String NO_REGISTERED_STORIES = "There are no registered stories.";

    public FilterStoryByStatusAndAssignee(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        List<String> teamAndBoardNameParameters = parameters.subList(0, 2);
        Board board = getTaskManagementRepository().getCurrentBoard(teamAndBoardNameParameters);
        Statusable status = ParsingHelpers.tryParseEnum(parameters.get(2), StoryStatus.class);
        String assigneeUsername = parameters.get(3);
        List<Story> stories = board.getStories();
        if (stories.isEmpty()) {
            throw new ElementNotFoundException(NO_REGISTERED_STORIES);
        }
        List<Story> filteredStories = filterList(stories, status, assigneeUsername);

        return ListingHelpers.storiesToString(filteredStories);
    }

    private List<Story> filterList(List<Story> stories, Statusable status, String assigneeUsername) {
        return stories.stream()
                .filter(story -> story.getStatus() == status && story.getAssignee().getUsername().equalsIgnoreCase(assigneeUsername))
                .collect(Collectors.toList());
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }
}
