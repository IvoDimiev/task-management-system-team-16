package operations.listingOperations;

import core.Contracts.TaskManagementRepository;
import exceptions.ElementNotFoundException;
import models.contracts.Board;
import models.tasks.contracts.Bug;
import models.tasks.contracts.Statusable;
import models.tasks.enums.BugStatus;
import operations.BaseOperation;
import utils.ListingHelpers;
import utils.ParsingHelpers;
import utils.ValidationHelpers;

import java.util.List;
import java.util.stream.Collectors;

public class FilterBugByStatusAndAssignee extends BaseOperation {
    public static final int EXPECTED_NUMBER_OF_PARAMETERS = 4;
    public static final String NO_REGISTERED_BUGS = "There are no registered bugs.";

    public FilterBugByStatusAndAssignee(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        List<String> teamAndBoardNameParameters = parameters.subList(0, 2);
        Board board = getTaskManagementRepository().getCurrentBoard(teamAndBoardNameParameters);
        Statusable status = ParsingHelpers.tryParseEnum(parameters.get(2), BugStatus.class);
        String assigneeUsername = parameters.get(3);
        List<Bug> bugs = board.getBugs();
        if (bugs.isEmpty()) {
            throw new ElementNotFoundException(NO_REGISTERED_BUGS);
        }
        List<Bug> filteredBugs = filterList(bugs, status, assigneeUsername);

        return ListingHelpers.bugsToString(filteredBugs);
    }


    private List<Bug> filterList(List<Bug> bugs, Statusable status, String assigneeUsername) {
        return bugs.stream()
                .filter(bug -> bug.getStatus() == status && bug.getAssignee().getUsername().equalsIgnoreCase(assigneeUsername))
                .collect(Collectors.toList());
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }
}
