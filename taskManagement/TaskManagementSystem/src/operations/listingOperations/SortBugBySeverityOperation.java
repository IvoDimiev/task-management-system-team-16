package operations.listingOperations;

import core.Contracts.TaskManagementRepository;
import exceptions.ElementNotFoundException;
import models.contracts.Board;
import models.tasks.contracts.Bug;
import operations.BaseOperation;
import utils.ListingHelpers;
import utils.ValidationHelpers;

import java.util.Comparator;
import java.util.List;

public class SortBugBySeverityOperation extends BaseOperation {
    public static final int EXPECTED_NUMBER_OF_PARAMETERS = 2;
    public static final String NO_REGISTERED_BUGS = "There are no registered bugs.";

    public SortBugBySeverityOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        List<String> teamAndBoardNameParameters = parameters.subList(0, 2);
        Board board = getTaskManagementRepository().getCurrentBoard(teamAndBoardNameParameters);
        List<Bug> bugs = board.getBugs();
        if (bugs.isEmpty()) {
            throw new ElementNotFoundException(NO_REGISTERED_BUGS);
        }
        compareTo(bugs);

        return ListingHelpers.bugsToString(bugs);
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }

    private void compareTo(List<Bug> bugs) {
        Comparator<Bug> sorted = Comparator.comparing(Bug::getSeverity);

        bugs.sort(sorted);
    }
}
