package operations.listingOperations;
import core.Contracts.TaskManagementRepository;
import exceptions.ElementNotFoundException;
import models.contracts.Board;
import models.tasks.contracts.Task;
import operations.BaseOperation;
import utils.ListingHelpers;
import utils.ValidationHelpers;

import java.util.Comparator;
import java.util.List;
public class SortAllByTitleOperation extends BaseOperation {
    public static final int EXPECTED_NUMBER_OF_PARAMETERS = 2;
    public static final String NO_REGISTERED_TASKS = "There are no registered tasks.";

    public SortAllByTitleOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        List<String> teamAndBoardNameParameters = parameters.subList(0, 2);
        Board board = getTaskManagementRepository().getCurrentBoard(teamAndBoardNameParameters);
        List<Task> tasks = board.getTasks();
        if (tasks.isEmpty()) {
            throw new ElementNotFoundException(NO_REGISTERED_TASKS);
        }
        compareTo(tasks);

        return ListingHelpers.tasksToString(tasks);
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }

    private <T extends Task> void compareTo(List<T> tasks) {
        Comparator<T> sorted = Comparator.comparing(T::getTitle);
        tasks.sort(sorted);
    }
}