package operations.listingOperations;

import core.Contracts.TaskManagementRepository;
import exceptions.ElementNotFoundException;
import models.contracts.Board;
import models.tasks.contracts.Feedback;
import models.tasks.contracts.Statusable;
import models.tasks.contracts.Task;
import models.tasks.enums.FeedbackStatus;
import operations.BaseOperation;
import utils.ListingHelpers;
import utils.ParsingHelpers;
import utils.ValidationHelpers;

import java.util.List;
import java.util.stream.Collectors;

public class FilterFeedbackByStatusOperation extends BaseOperation {
    public static final int EXPECTED_NUMBER_OF_PARAMETERS = 3;
    public static final String NO_REGISTERED_FEEDBACKS = "There are no registered feedbacks.";


    public FilterFeedbackByStatusOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        List<String> teamAndBoardNameParameters = parameters.subList(0, 2);
        Board board = getTaskManagementRepository().getCurrentBoard(teamAndBoardNameParameters);
        Statusable feedbackStatus = ParsingHelpers.tryParseEnum(parameters.get(2), FeedbackStatus.class);
        List<Feedback> allFeedbacks = board.getFeedback();
        if (allFeedbacks.isEmpty()) {
            throw new ElementNotFoundException(NO_REGISTERED_FEEDBACKS);
        }
        List<Feedback> filteredFeedbacks = filterList(feedbackStatus, allFeedbacks);

        return ListingHelpers.feedbackToString(filteredFeedbacks);
    }

    private <T extends Task> List<T> filterList(Statusable status, List<T> tasks) {
        return tasks.stream().filter(task -> task.getStatus().equals(status)).collect(Collectors.toList());
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }
}
