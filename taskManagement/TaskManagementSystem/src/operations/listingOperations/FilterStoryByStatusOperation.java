package operations.listingOperations;

import core.Contracts.TaskManagementRepository;
import exceptions.ElementNotFoundException;
import models.contracts.Board;
import models.tasks.contracts.Statusable;
import models.tasks.contracts.Story;
import models.tasks.contracts.Task;
import models.tasks.enums.StoryStatus;
import operations.BaseOperation;
import utils.ListingHelpers;
import utils.ParsingHelpers;
import utils.ValidationHelpers;

import java.util.List;
import java.util.stream.Collectors;

public class FilterStoryByStatusOperation extends BaseOperation {
    public static final int EXPECTED_NUMBER_OF_PARAMETERS = 3;
    public static final String NO_REGISTERED_STORIES = "There are no registered stories.";

    public FilterStoryByStatusOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        List<String> teamAndBoardNameParameters = parameters.subList(0, 2);
        Board board = getTaskManagementRepository().getCurrentBoard(teamAndBoardNameParameters);
        Statusable storyStatus = ParsingHelpers.tryParseEnum(parameters.get(2), StoryStatus.class);
        List<Story> allStories = board.getStories();
        if (allStories.isEmpty()) {
            throw new ElementNotFoundException(NO_REGISTERED_STORIES);
        }
        List<Story> filteredStories = filterList(storyStatus, allStories);

        return ListingHelpers.storiesToString(filteredStories);
    }

    private <T extends Task> List<T> filterList(Statusable status, List<T> tasks) {
        return tasks.stream().filter(task -> task.getStatus().equals(status)).collect(Collectors.toList());
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }
}
