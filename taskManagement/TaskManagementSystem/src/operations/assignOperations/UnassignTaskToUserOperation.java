package operations.assignOperations;

import core.Contracts.TaskManagementRepository;
import models.UserImpl;
import models.contracts.Board;
import models.contracts.User;
import models.tasks.contracts.Assignable;
import models.tasks.enums.UserRole;
import operations.BaseOperation;
import utils.ParsingHelpers;

import java.util.List;

public class UnassignTaskToUserOperation extends BaseOperation {
    public static final int EXPECTED_NUMBER_OF_PARAMETERS = 3;
    public static final String UNPARSABLE_ID = "The ID you put is unparsable or non-existent.";
    public static final String TASK_UNASSIGNED = "Task with id: %d is Unassigned";
    public static final String USER_UNASSIGNED_TASK = "%s unassigned user '%s' from task with ID: %d";
    public static final String UNASSIGNED_USER = "%s was unnasigned from task with ID: %d";
    public static final String UNASSIGNED_USERNAME = "Unassigned";
    public static final UserRole UNASSIGNED_USERROLE = UserRole.UNKNOWN;

    public UnassignTaskToUserOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        List<String> teamAndBoardNameParameters = parameters.subList(0,2);
        Board board = getTaskManagementRepository().getCurrentBoard(teamAndBoardNameParameters);
        int id = ParsingHelpers.tryParseInt(parameters.get(2), UNPARSABLE_ID);

        return unassignTask(id,board);
    }

    private String unassignTask(int id, Board board) {
        Assignable task = board.findAssignableById(id);
        User assignee = task.getAssignee();
        User loggedUser = getTaskManagementRepository().getLoggedInUser();
        loggedUser.createLog(String.format(USER_UNASSIGNED_TASK, loggedUser.getUsername(), assignee.getUsername(), task.getId()));
        assignee.createLog(String.format(UNASSIGNED_USER, assignee.getUsername(), task.getId()));
        task.putAssignee(new UserImpl(UNASSIGNED_USERNAME,UNASSIGNED_USERROLE));

        return (String.format(TASK_UNASSIGNED,id));
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }
}

