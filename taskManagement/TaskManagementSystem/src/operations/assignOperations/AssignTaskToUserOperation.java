package operations.assignOperations;

import core.Contracts.TaskManagementRepository;
import models.contracts.Board;
import models.contracts.User;
import models.tasks.contracts.Assignable;
import operations.BaseOperation;
import utils.ParsingHelpers;
import utils.ValidationHelpers;

import java.util.List;

public class AssignTaskToUserOperation extends BaseOperation {
    public static final int EXPECTED_NUMBER_OF_PARAMETERS = 4;
    public static final String UNPARSABLE_ID = "The ID you put is unparsable or non-existent.";
    public static final String TASK_ASSIGNED = "Task with id: %d was assigned to user: %s";
    public static final String USER_GOT_ASSIGNED = "%s got assigned to task with ID: %d by User: '%s'.";
    public static final String USER_ASSIGNED_TASK = "%s assigned new task to assignee: '%s'.";

    public AssignTaskToUserOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        List<String> teamAndBoardNameParameters = parameters.subList(0, 2);
        Board board = getTaskManagementRepository().getCurrentBoard(teamAndBoardNameParameters);
        int id = ParsingHelpers.tryParseInt(parameters.get(2), UNPARSABLE_ID);
        User assignee = getTaskManagementRepository().findUserByUsername(parameters.get(3));
        User loggedUser = getTaskManagementRepository().getLoggedInUser();

        return assignTask(id, assignee, loggedUser, board);
    }

    private String assignTask(int id, User assignee, User loggedUser, Board board) {
        Assignable task = board.findAssignableById(id);
        task.putAssignee(assignee);
        loggedUser.createLog(String.format(USER_ASSIGNED_TASK, loggedUser.getUsername(), assignee.getUsername()));
        assignee.createLog(String.format(USER_GOT_ASSIGNED, assignee.getUsername(), task.getId(), loggedUser.getUsername()));

        return (String.format(TASK_ASSIGNED, id, assignee.getUsername()));
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }
}
