package operations.changeOperations;

import core.Contracts.TaskManagementRepository;
import models.contracts.Board;
import models.contracts.Team;
import models.contracts.User;
import models.tasks.contracts.Bug;
import models.tasks.enums.Priority;
import operations.BaseOperation;
import utils.AuthenticationHelpers;
import utils.ParsingHelpers;
import utils.ValidationHelpers;

import java.util.List;

public class ChangeBugPriorityOperation extends BaseOperation {
    public static final int EXPECTED_NUMBER_OF_PARAMETERS = 4;
    public static final String UNPARSABLE_ID = "The ID you put is unparsable or non-existent.";
    public static final String BUG_CHANGED_PRIORITY = "Bug with ID: %d changed priority to '%s'.";
    public static final String USER_CHANGED_BUG_PRIORITY = "User: %s changed bug with ID: %d priority to '%s'. ";

    public ChangeBugPriorityOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        String boardName = parameters.get(0);
        String teamName = parameters.get(1);
        Team team = getTaskManagementRepository().findTeamByName(teamName);
        Board board = team.findBoardByName(boardName);
        int id = ParsingHelpers.tryParseInt(parameters.get(2), UNPARSABLE_ID);
        Priority priority = ParsingHelpers.tryParseEnum(parameters.get(3), Priority.class);
        User user = getTaskManagementRepository().getLoggedInUser();
        AuthenticationHelpers.validateLoggedUserInfo(user.getUsername(), team);

        return switchBugPriority(id, priority, user, board);
    }

    private String switchBugPriority(int id, Priority priority, User user, Board board) {
        Bug bug = board.findBugById(id);
        bug.changePriority(priority);
        user.createLog(String.format
                (USER_CHANGED_BUG_PRIORITY, user.getUsername(), id, priority));

        return String.format(BUG_CHANGED_PRIORITY, id, priority);
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }
}
