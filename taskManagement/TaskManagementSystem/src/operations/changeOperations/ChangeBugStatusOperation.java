package operations.changeOperations;

import core.Contracts.TaskManagementRepository;
import models.contracts.Board;
import models.contracts.Team;
import models.contracts.User;
import models.tasks.contracts.Bug;
import models.tasks.contracts.Statusable;
import models.tasks.enums.BugStatus;
import operations.BaseOperation;
import utils.AuthenticationHelpers;
import utils.ParsingHelpers;
import utils.ValidationHelpers;

import java.util.List;

public class ChangeBugStatusOperation extends BaseOperation {
    public static final int EXPECTED_NUMBER_OF_PARAMETERS = 4;
    public static final String UNPARSABLE_ID = "The ID you put is unparsable or non-existent.";
    public static final String BUG_CHANGED_STATUS = "Bug with id: %d changed status to '%s'.";
    public static final String USER_CHANGED_BUG_STATUS = "User: %s changed bug with ID: %d status to '%s'. ";

    public ChangeBugStatusOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        String boardName = parameters.get(0);
        String teamName = parameters.get(1);
        Team team = getTaskManagementRepository().findTeamByName(teamName);
        Board board = team.findBoardByName(boardName);
        int id = ParsingHelpers.tryParseInt(parameters.get(2), UNPARSABLE_ID);
        Statusable status = ParsingHelpers.tryParseEnum(parameters.get(3), BugStatus.class);
        User user = getTaskManagementRepository().getLoggedInUser();
        AuthenticationHelpers.validateLoggedUserInfo(user.getUsername(), team);

        return switchBugStatus(id, status, user, board);
    }

    private String switchBugStatus(int id, Statusable status, User user, Board board) {
        Bug bug = board.findBugById(id);
        bug.changeStatus(status);
        user.createLog(String.format
                (USER_CHANGED_BUG_STATUS, user.getUsername(), id, status));

        return String.format(BUG_CHANGED_STATUS, id, status);
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }
}
