package operations.changeOperations;

import core.Contracts.TaskManagementRepository;
import models.contracts.Board;
import models.contracts.Team;
import models.contracts.User;
import models.tasks.contracts.Feedback;
import operations.BaseOperation;
import utils.AuthenticationHelpers;
import utils.ParsingHelpers;
import utils.ValidationHelpers;

import java.util.List;

public class ChangeFeedbackRatingOperation extends BaseOperation {
    public static final int EXPECTED_NUMBER_OF_PARAMETERS = 4;
    public static final String UNPARSABLE_ID = "The ID you put is unparsable or non-existent.";
    public static final String UNPARSABLE_RATING = "The rating you put is unparsable.";
    public static final String FEEDBACK_CHANGED_RATING = "Feedback with id: %d changed rating to %d.";
    public static final String USER_CHANGED_FEEDBACK_RATING = "User: %s changed feedback with ID: %d feedback to '%s'. ";

    public ChangeFeedbackRatingOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        String boardName = parameters.get(0);
        String teamName = parameters.get(1);
        Team team = getTaskManagementRepository().findTeamByName(teamName);
        Board board = team.findBoardByName(boardName);
        int id = ParsingHelpers.tryParseInt(parameters.get(2), UNPARSABLE_ID);
        int rating = ParsingHelpers.tryParseInt(parameters.get(3), UNPARSABLE_RATING);
        User user = getTaskManagementRepository().getLoggedInUser();
        AuthenticationHelpers.validateLoggedUserInfo(user.getUsername(), team);

        return switchFeedbackRating(id, rating, user, board);
    }

    private String switchFeedbackRating(int id, int rating, User user, Board board) {
        Feedback feedback = board.findFeedbackById(id);
        feedback.changeRating(rating);
        user.createLog(String.format
                (USER_CHANGED_FEEDBACK_RATING, user.getUsername(), id, feedback));

        return String.format(FEEDBACK_CHANGED_RATING, id, rating);
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }
}
