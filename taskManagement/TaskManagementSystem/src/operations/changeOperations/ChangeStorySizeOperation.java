package operations.changeOperations;

import core.Contracts.TaskManagementRepository;
import models.contracts.Board;
import models.contracts.Team;
import models.contracts.User;
import models.tasks.contracts.Story;
import models.tasks.enums.Size;
import operations.BaseOperation;
import utils.AuthenticationHelpers;
import utils.ParsingHelpers;
import utils.ValidationHelpers;

import java.util.List;

public class ChangeStorySizeOperation extends BaseOperation {
    public static final int EXPECTED_NUMBER_OF_PARAMETERS = 4;
    public static final String UNPARSABLE_ID = "The ID you put is unparsable or non-existent.";
    public static final String STORY_CHANGED_SIZE = "Story with id: %d changed size to '%s'.";
    public static final String USER_CHANGED_STORY_SIZE = "User: %s changed story with ID: %d size to '%s'. ";

    public ChangeStorySizeOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        String boardName = parameters.get(0);
        String teamName = parameters.get(1);
        Team team = getTaskManagementRepository().findTeamByName(teamName);
        Board board = team.findBoardByName(boardName);
        int id = ParsingHelpers.tryParseInt(parameters.get(2), UNPARSABLE_ID);
        Size size = ParsingHelpers.tryParseEnum(parameters.get(3), Size.class);
        User user = getTaskManagementRepository().getLoggedInUser();
        AuthenticationHelpers.validateLoggedUserInfo(user.getUsername(), team);

        return switchStorySize(id, size, user, board);
    }

    private String switchStorySize(int id, Size size, User user, Board board) {
        Story story = board.findStoryById(id);
        story.changeSize(size);
        user.createLog(String.format
                (USER_CHANGED_STORY_SIZE, user.getUsername(), id, size));

        return String.format(STORY_CHANGED_SIZE, id, size);
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }
}
