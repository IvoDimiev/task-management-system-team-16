package operations.changeOperations;

import core.Contracts.TaskManagementRepository;
import models.contracts.Board;
import models.contracts.Team;
import models.contracts.User;
import models.tasks.contracts.Bug;
import models.tasks.enums.Severity;
import operations.BaseOperation;
import utils.AuthenticationHelpers;
import utils.ParsingHelpers;
import utils.ValidationHelpers;

import java.util.List;

public class ChangeBugSeverityOperation extends BaseOperation {
    public static final int EXPECTED_NUMBER_OF_PARAMETERS = 4;
    public static final String UNPARSABLE_ID = "The ID you put is unparsable or non-existent.";
    public static final String BUG_CHANGED_SEVERITY = "Bug with id: %d changed priority to '%s'.";
    public static final String USER_CHANGED_BUG_SEVERITY = "User: %s changed bug with ID: %d severity to '%s'. ";

    public ChangeBugSeverityOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);

    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        String boardName = parameters.get(0);
        String teamName = parameters.get(1);
        Team team = getTaskManagementRepository().findTeamByName(teamName);
        Board board = team.findBoardByName(boardName);
        int id = ParsingHelpers.tryParseInt(parameters.get(2), UNPARSABLE_ID);
        Severity severity = ParsingHelpers.tryParseEnum(parameters.get(3), Severity.class);
        User user = getTaskManagementRepository().getLoggedInUser();
        AuthenticationHelpers.validateLoggedUserInfo(user.getUsername(), team);

        return switchBugSeverity(id, severity, user, board);
    }

    private String switchBugSeverity(int id, Severity severity, User user, Board board) {
        Bug bug = board.findBugById(id);
        bug.changeSeverity(severity);
        user.createLog(String.format
                (USER_CHANGED_BUG_SEVERITY, user.getUsername(), id, severity));

        return String.format(BUG_CHANGED_SEVERITY, id, severity);
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }
}
