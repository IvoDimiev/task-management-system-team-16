package operations.showOperations;

import core.Contracts.TaskManagementRepository;
import models.contracts.Team;
import operations.BaseOperation;

import java.util.List;

public class ShowAllTeamsOperation extends BaseOperation {

    public static final String TEAMS_HEADER = "--TEAMS--";

    public ShowAllTeamsOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        return showAllTeams();
    }

    private String showAllTeams() {
        StringBuilder builder = new StringBuilder();
        builder.append(TEAMS_HEADER).append(System.lineSeparator());
        int counter = 1;
        for (Team team : getTaskManagementRepository().getTeams()) {
            builder.append(String.format("%d. %s", counter, team.toString()))
                    .append(System.lineSeparator());
            counter++;
        }

        return builder.toString().trim();
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }
}
