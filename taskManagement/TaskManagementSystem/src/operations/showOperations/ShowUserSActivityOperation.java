package operations.showOperations;

import core.Contracts.TaskManagementRepository;
import models.contracts.User;
import operations.BaseOperation;
import utils.ValidationHelpers;

import java.util.List;

public class ShowUserSActivityOperation extends BaseOperation {
    public static final int EXPECTED_NUMBER_OF_PARAMETERS = 1;
    public static final String USER_ACTIVITY_HEADER = "--USER ACTIVITY HISTORY--" +
            "\n -------------------";

    public ShowUserSActivityOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        String personToShow = parameters.get(0);

        return showPersonSActivity(personToShow);
    }

    private String showPersonSActivity(String personToShow) {
        User existingUser = getTaskManagementRepository().findUserByUsername(personToShow);
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append(USER_ACTIVITY_HEADER);
        existingUser.getHistory().forEach(strBuilder::append);

        return strBuilder.toString();
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }

}
