package operations.showOperations;

import core.Contracts.TaskManagementRepository;
import models.contracts.Board;
import models.contracts.Team;
import operations.BaseOperation;
import utils.ValidationHelpers;

import java.util.List;

public class ShowAllTeamBoardsOperation extends BaseOperation {
    public static final int EXPECTED_NUMBER_OF_PARAMETERS = 1;
    private final static String YOU_ARE_NOT_AN_ADMIN = "You are not an admin!";
    private final static String NO_SUCH_TEAM = "Team with name %s do not exists";
    public static final String TEAM_AND_BOARDS_HEADER = "--TEAM %s, BOARDS: --";

    public ShowAllTeamBoardsOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        String teamName = parameters.get(0);

        return showAllBoards(teamName);
    }

    private String showAllBoards(String teamName) {
        if (!getTaskManagementRepository().getLoggedInUser().isAdmin()) {
            throw new IllegalArgumentException(YOU_ARE_NOT_AN_ADMIN);
        }
        Team existingTeam = getTaskManagementRepository().findTeamByName(teamName);
        StringBuilder builder = new StringBuilder();
        builder.append(String.format(TEAM_AND_BOARDS_HEADER, teamName)).append(System.lineSeparator());
        int counter = 1;
        for (Board board : existingTeam.getBoards()) {
            builder.append(String.format("%d. %s", counter, board.toString()))
                    .append(System.lineSeparator());
            counter++;
        }

        return builder.toString().trim();
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }
}
