package operations.showOperations;

import core.Contracts.TaskManagementRepository;
import models.contracts.Team;
import operations.BaseOperation;
import utils.ValidationHelpers;

import java.util.List;

public class ShowTeamSActivityOperation extends BaseOperation {
    public static final int EXPECTED_NUMBER_OF_PARAMETERS = 1;
    public static final String TEAM_ACTIVITY_HEADER = " --TEAM ACTIVITY HISTORY--" +
            "\n -------------------";

    public ShowTeamSActivityOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        String teamToShow = parameters.get(0);

        return showTeamSActivity(teamToShow);
    }

    private String showTeamSActivity(String teamToShow) {
        Team existingTeam = getTaskManagementRepository().findTeamByName(teamToShow);
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append(TEAM_ACTIVITY_HEADER);
        existingTeam.getHistory().forEach(strBuilder::append);

        return strBuilder.toString();
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }
}
