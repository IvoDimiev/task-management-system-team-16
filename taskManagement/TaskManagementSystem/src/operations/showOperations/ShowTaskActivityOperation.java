package operations.showOperations;

import core.Contracts.TaskManagementRepository;
import models.contracts.Board;
import models.tasks.contracts.Task;
import operations.BaseOperation;
import utils.ValidationHelpers;

import java.util.List;

public class ShowTaskActivityOperation extends BaseOperation {
    public static final int EXPECTED_NUMBER_OF_PARAMETERS = 3;
    public static final String BOARD_ACTIVITY_HEADER = "--TASK ACTIVITY HISTORY--" +
            "\n -------------------";

    public ShowTaskActivityOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        List<String> teamAndBoardNameParameters = parameters.subList(0,2);
        int taskId = Integer.parseInt(parameters.get(2));
        Board board = getTaskManagementRepository().getCurrentBoard(teamAndBoardNameParameters);
        Task task = board.findTaskById(taskId);

        return showTaskActivity(task);
    }

    private String showTaskActivity(Task task) {
        StringBuilder strBuilder = new StringBuilder(BOARD_ACTIVITY_HEADER);
        task.getHistory().forEach(strBuilder::append);

        return strBuilder.toString();
    }

    @Override
    protected boolean requiresLogin() {
        return false;
    }
}
