package operations.showOperations;

import core.Contracts.TaskManagementRepository;
import models.contracts.User;
import operations.BaseOperation;

import java.util.List;

public class ShowAllUsersOperation extends BaseOperation {

    public static final String USERS_HEADER = "--USERS--";

    public ShowAllUsersOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        return showAllUsers();
    }

    private String showAllUsers() {
        StringBuilder builder = new StringBuilder();
        builder.append(USERS_HEADER).append(System.lineSeparator());
        int counter = 1;
        for (User user : getTaskManagementRepository().getUsers()) {
            builder.append(String.format("%d. %s", counter, user.toString()))
                    .append(System.lineSeparator());
            counter++;
        }

        return builder.toString().trim();
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }
}
