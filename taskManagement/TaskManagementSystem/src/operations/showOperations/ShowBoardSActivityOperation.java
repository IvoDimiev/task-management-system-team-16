package operations.showOperations;

import core.Contracts.TaskManagementRepository;
import models.contracts.Board;
import models.contracts.Team;
import operations.BaseOperation;
import utils.ValidationHelpers;

import java.util.List;

public class ShowBoardSActivityOperation extends BaseOperation {
    public static final int EXPECTED_NUMBER_OF_PARAMETERS = 2;
    public static final String BOARD_ACTIVITY_HEADER = "--BOARD ACTIVITY HISTORY--" +
            "\n -------------------";

    public ShowBoardSActivityOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        String boardName = parameters.get(0);
        String teamName = parameters.get(1);
        Team team = getTaskManagementRepository().findTeamByName(teamName);

        return showBoardSActivity(boardName,team);
    }

    private String showBoardSActivity(String boardToShow,Team team) {
        Board existingBoard = team.findBoardByName(boardToShow);
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append(BOARD_ACTIVITY_HEADER);
        existingBoard.getHistory().forEach(strBuilder::append);

        return strBuilder.toString();
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }
}
