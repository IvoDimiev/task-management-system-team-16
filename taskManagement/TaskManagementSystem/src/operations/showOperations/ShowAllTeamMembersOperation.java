package operations.showOperations;

import core.Contracts.TaskManagementRepository;
import models.contracts.Team;
import models.contracts.User;
import operations.BaseOperation;
import utils.ValidationHelpers;

import java.util.List;

public class ShowAllTeamMembersOperation extends BaseOperation {
    public static final int EXPECTED_NUMBER_OF_PARAMETERS = 1;
    private final static String YOU_ARE_NOT_AN_ADMIN = "You are not an admin!";
    private final static String NO_SUCH_TEAM = "Team with name %s do not exists";
    public static final String TEAM_AND_USERS_HEADER = "--TEAM %s, USERS: --";

    public ShowAllTeamMembersOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        String teamName = parameters.get(0);

        return showAllMembers(teamName);
    }

    private String showAllMembers(String teamName) {
        if (!getTaskManagementRepository().getLoggedInUser().isAdmin()) {
            throw new IllegalArgumentException(YOU_ARE_NOT_AN_ADMIN);
        }
        Team existingTeam = getTaskManagementRepository().findTeamByName(teamName);
        StringBuilder builder = new StringBuilder();
        builder.append(String.format(TEAM_AND_USERS_HEADER, teamName)).append(System.lineSeparator());
        int counter = 1;
        for (User user : existingTeam.getUsers()) {
            builder.append(String.format("%d. %s", counter, user.toString()))
                    .append(System.lineSeparator());
            counter++;
        }

        return builder.toString().trim();
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }
}
