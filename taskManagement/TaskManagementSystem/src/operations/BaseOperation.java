package operations;

import core.Contracts.TaskManagementRepository;
import operations.contracts.Operation;

import java.util.List;

public abstract class BaseOperation implements Operation {
    private final TaskManagementRepository taskManagementRepository;
    private final static String USER_NOT_LOGGED = "You are not logged in! Please login first!";

    public BaseOperation(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    protected TaskManagementRepository getTaskManagementRepository() {
        return taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        if (requiresLogin() && !taskManagementRepository.hasLoggedInUser()) {
            throw new IllegalArgumentException(USER_NOT_LOGGED);
        }

        return executeOperation(parameters);
    }

    protected abstract String executeOperation(List<String> parameters);

    protected abstract boolean requiresLogin();
}
