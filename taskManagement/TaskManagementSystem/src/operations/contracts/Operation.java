package operations.contracts;

import java.util.List;

public interface Operation {
    String execute(List<String> parameters);
}
