package operations.createOperations;

import core.Contracts.TaskManagementRepository;
import models.contracts.Board;
import models.contracts.Team;
import models.contracts.User;
import models.tasks.contracts.Task;
import models.tasks.enums.Priority;
import models.tasks.enums.Size;
import operations.BaseOperation;
import utils.AuthenticationHelpers;
import utils.ParsingHelpers;
import utils.ValidationHelpers;

import java.util.List;

public class CreateStoryOperation extends BaseOperation {
    private static final int EXPECTED_NUMBER_OF_PARAMETERS = 6;
    private final static String STORY_CREATED = "Story with title '%s' is created!";
    public static final String USER_CREATED_STORY_LOG = "User: '%s' added new story with ID: %d";

    public CreateStoryOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        String teamName = parameters.get(0);
        Team team = getTaskManagementRepository().findTeamByName(teamName);
        String boardName = parameters.get(1);
        String storyTitle = parameters.get(2);
        String storyDescription = parameters.get(3);
        Priority storyPriority = ParsingHelpers.tryParseEnum(parameters.get(4), Priority.class);
        Size size = ParsingHelpers.tryParseEnum(parameters.get(5), Size.class);
        User user = getTaskManagementRepository().getLoggedInUser();
        String username = user.getUsername();
        AuthenticationHelpers.validateLoggedUserInfo(username, team);
        Board board = team.findBoardByName(boardName);

        return createStory(storyTitle, storyDescription, storyPriority, size, board, user);
    }

    private String createStory(String storyTitle, String storyDescription, Priority storyPriority,
                               Size size, Board board, User user) {
        Task story = getTaskManagementRepository().createStory(storyTitle, storyDescription, storyPriority, size, board);
        user.createLog(String.format(USER_CREATED_STORY_LOG, user.getUsername(), story.getId()));

        return String.format(STORY_CREATED, storyTitle);
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }
}
