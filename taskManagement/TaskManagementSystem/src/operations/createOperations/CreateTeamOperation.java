package operations.createOperations;
import core.Contracts.TaskManagementRepository;
import models.contracts.Team;
import models.contracts.User;
import operations.BaseOperation;
import utils.ValidationHelpers;
import java.util.ArrayList;
import java.util.List;
public class CreateTeamOperation extends BaseOperation {
    private final static String TEAM_NAME_ALREADY_EXIST = "Team '%s' already exist. Choose a different team name!";
    private final static String TEAM_CREATED = "Team with title: '%s' is created!";
    public static final int EXPECTED_NUMBER_OF_PARAMETERS = 1;
    public static final String USER_CREATED_TEAM_LOG = "User: '%s' created new team with title: %s";

    public CreateTeamOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        String name = parameters.get(0);
        List<User> users = new ArrayList<>();
        User loggedUser = getTaskManagementRepository().getLoggedInUser();
        users.add(loggedUser);

        return createTeam(users, name, loggedUser);
    }

    private String createTeam(List<User> users, String name, User user) {
        throwIfTeamNameExists(name);
        Team team = getTaskManagementRepository().createTeam(users, name);
        getTaskManagementRepository().addTeam(team);
        user.createLog(String.format(USER_CREATED_TEAM_LOG, user.getUsername(), team.getName()));

        return String.format(TEAM_CREATED, team.getName());
    }

    private void throwIfTeamNameExists(String name) {
        if (getTaskManagementRepository().getTeams().stream().anyMatch(t -> t.getName().equalsIgnoreCase(name))) {
            throw new IllegalArgumentException(String.format(TEAM_NAME_ALREADY_EXIST, name));
        }
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }
}