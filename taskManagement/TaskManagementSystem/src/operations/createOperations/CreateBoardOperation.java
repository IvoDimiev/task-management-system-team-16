package operations.createOperations;

import core.Contracts.TaskManagementRepository;
import models.contracts.Board;
import models.contracts.Team;
import models.contracts.User;
import operations.BaseOperation;
import utils.AuthenticationHelpers;
import utils.ValidationHelpers;

import java.util.List;

public class CreateBoardOperation extends BaseOperation {
    private final static String BOARD_NAME_ALREADY_EXIST = "Board %s already exist. Choose a different name!";
    private final static String BOARD_CREATED = "Board with title: '%s' is created!";
    public static final int EXPECTED_NUMBER_OF_PARAMETERS = 2;
    public static final String USER_CREATED_BOARD_LOG = "User: '%s' created board with title: '%s'.";

    public CreateBoardOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }


    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        String boardName = parameters.get(0);
        String teamName = parameters.get(1);
        Team team = getTaskManagementRepository().findTeamByName(teamName);
        User user = getTaskManagementRepository().getLoggedInUser();
        AuthenticationHelpers.validateLoggedUserInfo(user.getUsername(), team);

        return createBoard(boardName, teamName, team, user);
    }

    private String createBoard(String boardName, String teamName, Team team, User user) {
        throwIfBoardNameExists(boardName, teamName);
        Board board = getTaskManagementRepository().createBoard(boardName);
        String username = user.getUsername();
        team.addBoard(board);
        user.createLog(String.format(USER_CREATED_BOARD_LOG, username, board.getName()));

        return String.format(BOARD_CREATED, boardName);
    }

    private void throwIfBoardNameExists(String name, String teamName) {
        Team team = getTaskManagementRepository().findTeamByName(teamName);

        if (team.getBoards().stream().anyMatch(board -> board.getName().equals(name))) {
            throw new IllegalArgumentException(String.format(BOARD_NAME_ALREADY_EXIST, name));
        }
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }
}
