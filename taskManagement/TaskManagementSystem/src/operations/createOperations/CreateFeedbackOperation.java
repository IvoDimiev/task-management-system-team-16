package operations.createOperations;

import core.Contracts.TaskManagementRepository;
import models.contracts.Board;
import models.contracts.Team;
import models.contracts.User;
import models.tasks.contracts.Task;
import operations.BaseOperation;
import utils.AuthenticationHelpers;
import utils.ValidationHelpers;

import java.util.List;

public class CreateFeedbackOperation extends BaseOperation {
    private static final int EXPECTED_NUMBER_OF_PARAMETERS = 5;
    private final static String FEEDBACK_CREATED = "Feedback with title '%s' is created!";
    public static final String USER_CREATED_FEEDBACK_LOG = "User: '%s' added new feedback with ID: %d";

    public CreateFeedbackOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        String teamName = parameters.get(0);
        String boardName = parameters.get(1);
        String feedbackTitle = parameters.get(2);
        String feedbackDescription = parameters.get(3);
        int rating = Integer.parseInt(parameters.get(4));
        User user = getTaskManagementRepository().getLoggedInUser();
        String username = user.getUsername();
        Team team = getTaskManagementRepository().findTeamByName(teamName);
        AuthenticationHelpers.validateLoggedUserInfo(username, team);
        Board board = team.findBoardByName(boardName);

        return createFeedback(feedbackTitle, feedbackDescription, rating, board, user);
    }

    private String createFeedback(String feedbackTitle, String feedbackDescription, int rating, Board board, User user) {
        Task feedback = getTaskManagementRepository().createFeedback(feedbackTitle, feedbackDescription, rating, board);
        user.createLog(String.format(USER_CREATED_FEEDBACK_LOG, user.getUsername(), feedback.getId()));

        return String.format(FEEDBACK_CREATED, feedbackTitle);

    }


    @Override
    protected boolean requiresLogin() {
        return true;
    }
}
