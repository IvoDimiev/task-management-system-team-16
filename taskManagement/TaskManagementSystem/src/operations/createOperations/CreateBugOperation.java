package operations.createOperations;

import core.Contracts.TaskManagementRepository;
import models.contracts.Board;
import models.contracts.Team;
import models.contracts.User;
import models.tasks.contracts.Task;
import models.tasks.enums.Priority;
import models.tasks.enums.Severity;
import operations.BaseOperation;
import utils.AuthenticationHelpers;
import utils.ParsingHelpers;
import utils.ValidationHelpers;

import java.util.List;

public class CreateBugOperation extends BaseOperation {
    private static final int EXPECTED_NUMBER_OF_PARAMETERS = 7;
    private final static String BUG_CREATED = "Bug with title '%s' is created!";
    public static final String USER_CREATED_BUG_LOG = "User: '%s' added new bug with ID: %d";

    public CreateBugOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        String teamName = parameters.get(0);
        String boardName = parameters.get(1);
        String bugTitle = parameters.get(2);
        String bugDescription = parameters.get(3);
        Priority bugPriority = ParsingHelpers.tryParseEnum(parameters.get(4), Priority.class);
        Severity severity = ParsingHelpers.tryParseEnum(parameters.get(5), Severity.class);
        List<String> steps = List.of(parameters.get(6).split(";"));
        User user = getTaskManagementRepository().getLoggedInUser();
        String username = user.getUsername();
        Team team = getTaskManagementRepository().findTeamByName(teamName);
        AuthenticationHelpers.validateLoggedUserInfo(username, team);
        Board board = team.findBoardByName(boardName);

        return createBug(bugTitle, bugDescription, bugPriority, severity, steps, board, user);
    }

    private String createBug(String bugTitle, String bugDescription, Priority bugPriority,
                             Severity severity, List<String> steps, Board board, User user) {
        Task bug = getTaskManagementRepository().createBug(bugTitle, bugDescription, bugPriority, severity, steps, board);
        user.createLog(String.format(USER_CREATED_BUG_LOG, user.getUsername(), bug.getId()));

        return String.format(BUG_CREATED, bugTitle);
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }
}
