package operations.addOperations;

import core.Contracts.TaskManagementRepository;
import models.contracts.Board;
import models.contracts.Comment;
import models.contracts.User;
import models.tasks.contracts.Task;
import operations.BaseOperation;
import utils.ParsingHelpers;
import utils.ValidationHelpers;

import java.util.List;

public class AddCommentToATaskOperation extends BaseOperation {
    public static String VALID_INT_ERROR_MSG = "Please enter digit for task ID.";
    public static final int EXPECTED_NUMBER_OF_PARAMETERS = 4;

    public AddCommentToATaskOperation(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeOperation(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_PARAMETERS);
        List<String> teamAndBoardNameParameters = parameters.subList(0, 2);
        int taskId = ParsingHelpers.tryParseInt(parameters.get(2), VALID_INT_ERROR_MSG);
        String content = parameters.get(3);
        Board board = getTaskManagementRepository().getCurrentBoard(teamAndBoardNameParameters);
        User author = getTaskManagementRepository().getLoggedInUser();
        Task task = board.findTaskById(taskId);

        return addCommentToTask(content, author, task);
    }

    private String addCommentToTask(String content, User author, Task task) {
        Comment comment = getTaskManagementRepository().createComment(author, content);
        task.addComment(comment);
        author.createLog(String.format("%s added comment to a task %d", author.getUsername(), task.getId()));

        return String.format("Comment successfully added to task with id: %d", task.getId());
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }
}
