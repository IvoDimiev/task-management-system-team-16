package utils;

import models.contracts.Team;

public class AuthenticationHelpers {
    public static void validateLoggedUserInfo(String username, Team team){
        if(team.getUsers().stream().noneMatch(user -> user.getUsername().equalsIgnoreCase(username))){
            throw new IllegalArgumentException("You cannot create a task if you are not in the team!");
        }
    }
}
