package utils;


import models.tasks.contracts.*;

import java.util.List;
import java.util.stream.Collectors;

public class ListingHelpers {

    public static String tasksToString(List<Task> tasks) {
        return elementsToString(tasks);
    }

    public static String bugsToString(List<Bug> bugs) {
        return elementsToString(bugs);
    }

    public static String assignablesToString(List<Assignable> bugs) {
        return elementsToString(bugs);
    }

    public static String prioritisablesToString(List<Prioritisable> bugs) {
        return elementsToString(bugs);
    }

    public static String storiesToString(List<Story> stories) {
        return elementsToString(stories);
    }

    public static String feedbackToString(List<Feedback> feedbacks) {
        return elementsToString(feedbacks);
    }

    private static <T extends Printable> String elementsToString(List<T> elements) {
        return elements
                .stream()
                .map(Object::toString)
                .collect(Collectors.joining("\n###############" + System.lineSeparator()));
    }

}

