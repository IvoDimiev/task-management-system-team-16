package models;

import models.contracts.HistoryLog;
import models.contracts.User;
import models.tasks.enums.UserRole;
import utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;

public class UserImpl implements User {
    public static final int USERNAME_LEN_MIN = 5;
    public static final int USERNAME_LEN_MAX = 15;
    private static final String USERNAME_REGEX_PATTERN = "^[A-Za-z0-9]+$";
    private static final String USERNAME_PATTERN_ERR = "Username contains invalid symbols!";
    private static final String USERNAME_LEN_ERR = format("Username must be between %d and %d characters long!",
            USERNAME_LEN_MIN, USERNAME_LEN_MAX);
    public static final int PASS_MIN = 2;
    public static final int PASS_MAX = 15;
    public static final String PASS_ERROR_MESSAGE = format("Please enter a password between %d and %d symbols.",
            PASS_MIN, PASS_MAX);
    public final static String USER_TO_STRING = "Username: %s, Role: %s";
    public final static String USER_CREATED = "User: '%s' with Id: %d was created!";

    private int id;
    private String username;
    private String password;
    private UserRole role;
    private final List<HistoryLog> history;

    public UserImpl(String username,UserRole role) {
        setUsername(username);
        this.role = role;
        history = new ArrayList<>();
    }

    public UserImpl(int id, String username, String password, UserRole role) {
        this(username,role);
        this.id = id;

        setPassword(password);
        createLog(String.format(USER_CREATED, getUsername(), getId()));
    }

    @Override
    public String getName(){
        return getUsername();
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public List<HistoryLog> getHistory() {
        return new ArrayList<>(history);
    }

    @Override
    public void addToLog(HistoryLog log) {
        history.add(log);
    }

    @Override
    public void createLog(String message) {
        HistoryLog newLog = new HistoryLogImpl(message);
        addToLog(newLog);
    }

    @Override
    public String toString() {
        return String.format(
                USER_TO_STRING,
                username,
                role);
    }

    @Override
    public boolean isAdmin() {
        return role == UserRole.ADMIN;
    }

    @Override
    public void changePassword(String password) {
        setPassword(password);
    }

    private void setUsername(String username) {
        ValidationHelpers.validatePattern(username, USERNAME_REGEX_PATTERN, USERNAME_PATTERN_ERR);
        ValidationHelpers.validateIntRange(username.length(), USERNAME_LEN_MIN, USERNAME_LEN_MAX, USERNAME_LEN_ERR);
        this.username = username;
    }

    private void setPassword(String password) {
        ValidationHelpers.validateIntRange(password.length(), PASS_MIN, PASS_MAX, PASS_ERROR_MESSAGE);
        this.password = password;
    }
}
