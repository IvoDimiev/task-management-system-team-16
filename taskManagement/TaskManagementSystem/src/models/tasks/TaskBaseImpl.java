package models.tasks;

import models.HistoryLogImpl;
import models.contracts.Comment;
import models.contracts.HistoryLog;
import models.tasks.contracts.Statusable;
import models.tasks.contracts.Task;
import utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

public abstract class TaskBaseImpl implements Task {
    public static final int NAME_MIN_VALUE = 10;
    public static final int NAME_MAX_VALUE = 50;
    private static final String NAME_LENGTH_ERROR = String.format(
            "Task name should be between %d and %d characters!",
            NAME_MIN_VALUE, NAME_MAX_VALUE);

    public static final int DESCRIPTION_MIN_VALUE = 10;
    public static final int DESCRIPTION_MAX_VALUE = 500;
    private static final String DESCRIPTION_LENGTH_ERROR = String.format(
            "Task description should be between %d and %d characters!",
            DESCRIPTION_MIN_VALUE, DESCRIPTION_MAX_VALUE);
    public final static String COMMENTS_HEADER = "--COMMENTS--";
    public final static String NO_COMMENTS_HEADER = "--NO COMMENTS--";
    private static final String COMMENTS_BODY = "--END COMMENTS--";
    public static final String COMMENT_ADDED = "Comment was added to task!";
    public static final String GENERAL_INFO = "%s:%n" +
            "ID: %d%n" +
            "Title: %s%n" +
            "Description: %s%n" +
            "Status: %s";
    private String title;
    private String description;
    private Statusable status;
    private final List<Comment> comments;
    private final List<HistoryLog> history;
    private int id;

    public TaskBaseImpl(int id, String title, String description, Statusable status) {
        this.id = id;
        setTitle(title);
        setDescription(description);
        setStatus(status);
        comments = new ArrayList<>();
        history = new ArrayList<>();
    }

    @Override
    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public Statusable getStatus() {
        return status;
    }

    @Override
    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void changeStatus(Statusable status) {
        setStatus(status);
    }

    public void addToLog(HistoryLog log) {
        history.add(log);
    }

    @Override
    public void addComment(Comment comment) {
        HistoryLog log = new HistoryLogImpl(COMMENT_ADDED);
        addToLog(log);
        comments.add(comment);
    }

    public abstract void createLog(String message);

    @Override
    public List<HistoryLog> getHistory() {
        return new ArrayList<>(history);
    }

    protected String generalInfo() {
        return String.format(GENERAL_INFO, getClass().getSimpleName().replace("Impl", ""),
                getId(), getTitle(), getDescription(), getStatus());
    }

    protected String getCommentsInfo() {
        StringBuilder strBuilder = new StringBuilder();
        if (getComments().size() == 0) {
            return NO_COMMENTS_HEADER + "\n";
        }
        strBuilder.append(COMMENTS_HEADER);
        getComments().forEach(strBuilder::append);
        strBuilder.append(COMMENTS_BODY);
        strBuilder.append(System.lineSeparator());

        return strBuilder.toString();
    }

    private void setStatus(Statusable status) {
        this.status = status;
    }

    private void setTitle(String title) {
        ValidationHelpers.validateIntRange(title.length(), NAME_MIN_VALUE, NAME_MAX_VALUE, NAME_LENGTH_ERROR);
        this.title = title;
    }

    private void setDescription(String description) {
        ValidationHelpers.validateIntRange(description.length(), DESCRIPTION_MIN_VALUE, DESCRIPTION_MAX_VALUE, DESCRIPTION_LENGTH_ERROR);
        this.description = description;
    }
}
