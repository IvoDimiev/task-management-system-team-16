package models.tasks;

import exceptions.InvalidUserInputException;
import models.HistoryLogImpl;
import models.UserImpl;
import models.contracts.HistoryLog;
import models.contracts.User;
import models.tasks.contracts.Statusable;
import models.tasks.contracts.Story;
import models.tasks.enums.Priority;
import models.tasks.enums.Size;
import models.tasks.enums.StoryStatus;
import models.tasks.enums.UserRole;

public class StoryImpl extends TaskBaseImpl implements Story {
    public static final UserImpl UNASSIGNED_USER = new UserImpl("Unassigned", UserRole.UNKNOWN);
    public static final String STORY_TO_STRING = "%s%n" +
            "Assignee: %s%n" +
            "Size: %s%n" +
            "Priority: %s%n" +
            "Comments: %s";
    public static final String ERROR_ASSIGNEE = "You have to choose a different Assignee. This one is already set.";
    public static final String ERROR_SIZE = "You have to choose a different size. This one is already set.";
    public static final String ERROR_STATUS = "You have to choose a different status. This one is already set.";
    public static final String STATUS_LOG = "Status of Story id: %d changed from %s to %s";
    public static final String ASSIGNEE_LOG = "Assignee of Story id: %d changed from %s to %s";
    public static final String STORY_CREATION_LOG = "Story with title: %s and id: %d was created!";
    private Priority priority;
    private Size size;
    private User assignee;
    private final static String PRIORITY_CHANGED = "Priority changed from %s to %s";
    private final static String SIZE_CHANGED = "Priority changed from %s to %s";
    private final static String ERROR_PRIORITY = "Priority can't be changed from %s to %s";

    public StoryImpl(int id, String title, String description, Priority priority, Size size) {
        super(id, title, description, StoryStatus.NOT_DONE);
        setPriority(priority);
        setSize(size);
        setAssignee(UNASSIGNED_USER);

        createLog(String.format(STORY_CREATION_LOG, getTitle(), getId()));
    }

    @Override
    public Priority getPriority() {
        return priority;
    }

    @Override
    public Size getSize() {
        return size;
    }

    @Override
    public User getAssignee() {
        return assignee;
    }

    @Override
    public void createLog(String message) {
        HistoryLog newLog = new HistoryLogImpl(message);
        addToLog(newLog);
    }

    public void changePriority(Priority priority) {
        if (this.priority != priority) {
            createLog(String.format(PRIORITY_CHANGED, getPriority(), priority));
            setPriority(priority);
        } else {
            throw new IllegalArgumentException(String.format(ERROR_PRIORITY, getPriority(), priority));
        }
    }

    @Override
    public void changeStatus(Statusable status) {
        if (getStatus() != status) {
            createLog(String.format(STATUS_LOG, getId(), getStatus(), status));
            super.changeStatus(status);
        } else {
            throw new InvalidUserInputException(ERROR_STATUS);
        }
    }

    @Override
    public void changeSize(Size size) {
        if (this.size != size) {
            createLog(String.format(SIZE_CHANGED, getSize(), size));
            setSize(size);
        } else {
            throw new InvalidUserInputException(ERROR_SIZE);
        }
    }

    @Override
    public void putAssignee(User assignee) {
        if (getAssignee() != assignee) {
            createLog(String.format(ASSIGNEE_LOG, getId(), getAssignee(), assignee));
            setAssignee(assignee);
        } else {
            throw new InvalidUserInputException(ERROR_ASSIGNEE);
        }
    }

    @Override
    public String toString() {
        return String.format(STORY_TO_STRING, generalInfo(), getAssignee().getUsername(), getSize(), getPriority(), getCommentsInfo());
    }

    private void setSize(Size size) {
        this.size = size;
    }

    private void setPriority(Priority priority) {
        this.priority = priority;
    }

    private void setAssignee(User assignee) {
        this.assignee = assignee;
    }
}
