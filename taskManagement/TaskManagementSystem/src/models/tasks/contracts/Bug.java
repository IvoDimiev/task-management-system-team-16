package models.tasks.contracts;

import models.tasks.enums.Severity;

import java.util.List;

public interface Bug extends Task,Prioritisable,Assignable{
    void changeSeverity(Severity severity);

    Severity getSeverity();

    List<String> getSteps();
}
