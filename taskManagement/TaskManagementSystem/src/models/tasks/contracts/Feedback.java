package models.tasks.contracts;

public interface Feedback extends Task {
    void changeRating(int rating);

    int getRating();
}
