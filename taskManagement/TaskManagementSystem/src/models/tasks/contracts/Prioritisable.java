package models.tasks.contracts;

import models.tasks.enums.Priority;

public interface Prioritisable extends Printable{
    void changePriority(Priority priority);

    Priority getPriority();
}
