package models.tasks.contracts;

import models.tasks.enums.Size;

public interface Story extends Task,Prioritisable,Assignable{
    void changeSize(Size size);

    Size getSize();
}
