package models.tasks.contracts;

import models.contracts.Identifiable;
import models.contracts.User;

public interface Assignable extends Identifiable, Printable {
    void putAssignee (User assignee);

    User getAssignee();
}
