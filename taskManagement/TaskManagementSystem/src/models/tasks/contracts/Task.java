package models.tasks.contracts;

import models.contracts.Comment;
import models.contracts.Identifiable;
import models.contracts.Loggable;

import java.util.List;

public interface Task extends Printable, Loggable, Identifiable, Statusable {
    void changeStatus(Statusable status);

    String getTitle();

    void addComment(Comment comment);

    Statusable getStatus();

    List<Comment> getComments();

    String getDescription();
}
