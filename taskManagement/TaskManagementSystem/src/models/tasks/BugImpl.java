package models.tasks;

import exceptions.InvalidUserInputException;
import models.HistoryLogImpl;
import models.UserImpl;
import models.contracts.HistoryLog;
import models.contracts.User;
import models.tasks.contracts.Bug;
import models.tasks.contracts.Statusable;
import models.tasks.enums.BugStatus;
import models.tasks.enums.Priority;
import models.tasks.enums.Severity;
import models.tasks.enums.UserRole;

import java.util.Arrays;
import java.util.List;

public class BugImpl extends TaskBaseImpl implements Bug {
    public static final String BUG_CHANGE_ASSIGNEE_ERROR = "This assignee is already assigned!";
    private final static String PRIORITY_CHANGED = "Priority changed from %s to %s";
    public static final UserImpl UNASSIGNED_USER = new UserImpl("Unassigned", UserRole.UNKNOWN);
    public static final String BUG_CREATED_LOG = "Bug with title: %s and id: %d was created!";
    public static final String BUG_TO_STRING = "%s%n" +
            "Severity: %s%n" +
            "Assignee: %s%n" +
            "Priority: %s%n" +
            "Steps: %s%n" +
            "%s";
    public static final String BUG_CHANGE_PRIORITY_ERROR = "Priority is currently a %s";
    public static final String BUG_CHANGE_SEVERITY_ERROR = "Severity is currently a %s";
    public static final String BUG_STATUS_CHANGE_ERROR = "Status is currently a %s";
    public static final String BUG_STATUS_CHANGED = "Status of Bug id: %d changed from %s to %s";
    public static final String BUG_ASSIGNEE_CHANGED = "Assignee of Bug id: %d changed from %s to %s";

    private Priority priority;
    private Severity severity;
    private User assignee;
    private final List<String> steps;

    public BugImpl(int id, String title, String description, Priority priority, Severity severity, List<String> steps) {
        super(id, title, description, BugStatus.ACTIVE);
        setPriority(priority);
        setSeverity(severity);
        setAssignee(UNASSIGNED_USER);
        this.steps = steps;

        createLog(String.format(BUG_CREATED_LOG,getTitle(),getId()));
    }

    @Override
    public Priority getPriority() {
        return priority;
    }

    @Override
    public Severity getSeverity() {
        return severity;
    }

    @Override
    public User getAssignee() {
        return assignee;
    }

    @Override
    public List<String> getSteps() {
        return steps;
    }

    @Override
    public void createLog(String message) {
        HistoryLog newLog = new HistoryLogImpl(message);
        addToLog(newLog);
    }

    @Override
    public void changePriority(Priority priority) {
        if(this.priority!= priority){
            createLog(String.format(PRIORITY_CHANGED,getPriority(),priority));
            setPriority(priority);
        }else{
            throw new InvalidUserInputException(String.format(BUG_CHANGE_PRIORITY_ERROR, getPriority().toString()));
        }
    }

    @Override
    public void changeSeverity(Severity severity) {
        if(this.severity!=severity){
            createLog(String.format(PRIORITY_CHANGED,getPriority(),priority));
            setSeverity(severity);
        }else{
            throw new InvalidUserInputException(String.format(BUG_CHANGE_SEVERITY_ERROR, getSeverity().toString()));
        }
    }

    @Override
    public void putAssignee(User assignee) {
        if(getAssignee()!=assignee){
            createLog(String.format(BUG_ASSIGNEE_CHANGED, getId(), getAssignee(),assignee));
            setAssignee(assignee);
        }else{
            throw new IllegalArgumentException(BUG_CHANGE_ASSIGNEE_ERROR);

        }
    }

    @Override
    public void changeStatus(Statusable status) {
        if(getStatus()!=status) {
            createLog(String.format(BUG_STATUS_CHANGED, getId(), getStatus(), status));
            super.changeStatus(status);
        }else{
            throw new IllegalArgumentException(String.format(BUG_STATUS_CHANGE_ERROR, getStatus()));
        }
    }

    @Override
    public String toString() {
        return String.format(BUG_TO_STRING, generalInfo(), getSeverity(),
                getAssignee().getUsername(),getPriority(), Arrays.toString(getSteps().toArray())
                        .replace("[","").replace("]",""), getCommentsInfo());
    }

    private void setPriority(Priority priority) {
        this.priority = priority;
    }

    private void setSeverity(Severity severity) {
        this.severity = severity;
    }

    private void setAssignee(User assignee) {
        this.assignee = assignee;
    }
}
