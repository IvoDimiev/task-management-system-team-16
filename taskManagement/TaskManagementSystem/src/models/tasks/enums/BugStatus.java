package models.tasks.enums;

import models.tasks.contracts.Statusable;

public enum BugStatus implements Statusable {
    ACTIVE,
    FIXED;

    @Override
    public String toString() {
        switch (this) {
            case ACTIVE:
                return "Active";
            case FIXED:
                return "Fixed";
            default:
                return "Missing size enum";
        }
    }
}
