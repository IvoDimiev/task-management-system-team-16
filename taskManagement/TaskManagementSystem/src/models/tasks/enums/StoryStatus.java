package models.tasks.enums;

import models.tasks.contracts.Statusable;

public enum StoryStatus implements Statusable {
    NOT_DONE,
    IN_PROGRESS,
    DONE;

    @Override
    public String toString() {
        switch (this) {
            case NOT_DONE:
                return "Not done";
            case IN_PROGRESS:
                return "In progress";
            case DONE:
                return "Done";
            default:
                return "Missing status enum";
        }
    }
}
