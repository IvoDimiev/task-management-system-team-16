package models.tasks.enums;

import models.tasks.contracts.Statusable;

public enum FeedbackStatus implements Statusable {
    NEW,
    UNSCHEDULED,
    SCHEDULED,
    DONE;

    @Override
    public String toString() {
        switch (this) {
            case NEW:
                return "New";
            case UNSCHEDULED:
                return "Unscheduled";
            case SCHEDULED:
                return "Scheduled";
            case DONE:
                return "Done";
            default:
                return "Missing status enum";
        }
    }
}
