package models.tasks.enums;

public enum UserRole {
    DEVELOPER,
    ADMIN,
    UNKNOWN;

    @Override
    public String toString() {
        switch (this) {
            case DEVELOPER:
                return "Developer";
            case ADMIN:
                return "Admin";
            case UNKNOWN:
                return "Unknown";
            default:
                return "Missing size enum";
        }
    }
}
