package models.tasks;

import exceptions.InvalidUserInputException;
import models.HistoryLogImpl;
import models.contracts.HistoryLog;
import models.tasks.contracts.Feedback;
import models.tasks.contracts.Statusable;
import models.tasks.enums.FeedbackStatus;
import utils.ValidationHelpers;

public class FeedbackImpl extends TaskBaseImpl implements Feedback {
    public static final int RATING_MIN_VALUE = 10;
    public static final int RATING_MAX_VALUE = 50;
    private static final String RATING_ERROR = String.format(
            "Rating should be in the range %d - %d",
            RATING_MIN_VALUE, RATING_MAX_VALUE);
    public static final String FEEDBACK_TO_STRING = "%s%n" +
            "Rating: %s%n" +
            "Comments: %s";
    public static final String FEEDBACK_CREATION_LOG = "Feedback with title: %s and id: %d was created!";
    public static final String FEEDBACK_STATUS_LOG = "Status of Feedback id: %d changed from %s to %s";
    public static final String FEEDBACK_STATUS_ERROR = "You have to choose a different status. This one is already set.";
    public static final String FEEDBACK_RATING_ERROR = "You cannot change the rating without changing its value";
    public static final String FEEDBACK_CHANGE_RATING_LOG = "Rating of Feedback id: %d changed from %d to %d";

    private int rating;

    public FeedbackImpl(int id, String title, String description, int rating) {
        super(id, title, description, FeedbackStatus.NEW);
        setRating(rating);
        createLog(String.format(FEEDBACK_CREATION_LOG,getTitle(),getId()));
    }

    @Override
    public int getRating() {
        return rating;
    }

    @Override
    public void createLog(String message) {
        HistoryLog newLog = new HistoryLogImpl(message);
        addToLog(newLog);
    }

    @Override
    public void changeRating(int rating) {
        if(this.rating!=rating){
            createLog(String.format(FEEDBACK_CHANGE_RATING_LOG,getId(),getRating(),rating));
            setRating(rating);
        }else{
            throw new InvalidUserInputException(FEEDBACK_RATING_ERROR);
        }
    }

    @Override
    public void changeStatus(Statusable status) {
        if(getStatus()!=status) {
            createLog(String.format(FEEDBACK_STATUS_LOG, getId(), getStatus(), status));
            super.changeStatus(status);
        }else{
            throw new InvalidUserInputException(FEEDBACK_STATUS_ERROR);
        }
    }

    @Override
    public String toString() {
        return String.format(FEEDBACK_TO_STRING, generalInfo(), getRating(), getCommentsInfo());
    }

    private void setRating(int rating) {
        ValidationHelpers.validateIntRange(rating, RATING_MIN_VALUE, RATING_MAX_VALUE, RATING_ERROR);
        this.rating = rating;
    }
}
