package models;

import exceptions.InvalidUserInputException;
import models.contracts.HistoryLog;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class HistoryLogImpl implements HistoryLog {
    public static final String DATE_PATTERN = "yyyy/MM/dd HH:mm";
    public static final String LOG_TO_STRING = "\n[%s] %s" +
            "\n -------------------";
    private final String description;
    private final LocalDateTime timestamp;
    private final static String WRONG_DESCRIPTION = "Description cannot be empty.";

    public HistoryLogImpl(String description) {
        this.description = description;
        timestamp = LocalDateTime.now();
    }

    @Override
    public String getDescription() {
        if (description == null) {
            throw new InvalidUserInputException(WRONG_DESCRIPTION);
        }
        return description;
    }

    private String getTimestamp() {
        return timestamp.format(DateTimeFormatter.ofPattern(DATE_PATTERN));
    }

    public String toString() {
        return String.format(
                LOG_TO_STRING,
                getTimestamp(), getDescription());
    }
}
