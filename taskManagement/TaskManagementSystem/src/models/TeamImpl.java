package models;

import exceptions.ElementNotFoundException;
import models.contracts.*;
import utils.ValidationHelpers;
import java.util.ArrayList;
import java.util.List;

public class TeamImpl implements Team {
    public static final int NAME_MIN_VALUE = 5;
    public static final int NAME_MAX_VALUE = 15;
    private static final String NAME_LENGTH_ERROR = String.format(
            "Team name should be between %d and %d characters!",
            NAME_MIN_VALUE, NAME_MAX_VALUE);
    public final static String TEAM_TO_STRING = "Team: %s";
    public static final String TEAM_CREATED = "Team with name: '%s' was created!";
    public static final String BOARD_ADDED_TO_TEAM_BOARDLIST = "Board with name: '%s' was added to team: '%s' 's board list!";
    public static final String USER_ADDED_TO_TEAM = "User: '%s' was added to team: '%s'!";
    public static final String BOARD_TYPE = "board";
    public static final String ELEMENT_NOT_FOUND_ERROR = "No %s with title %s";

    private final List<User> users;
    private String name;
    private final List<Board> boards;
    private final List<HistoryLog> history;

    public TeamImpl(List<User> users, String name) {
        this.users = users;
        boards = new ArrayList<>();
        history = new ArrayList<>();
        setName(name);
        createLog(String.format(TEAM_CREATED,getName()));
    }

    public List<User> getUsers() {
        return new ArrayList<>(users);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Board> getBoards() {
        return new ArrayList<>(boards);
    }

    @Override
    public void addBoard(Board board) {
        boards.add(board);
        createLog(String.format(BOARD_ADDED_TO_TEAM_BOARDLIST,board.getName(),getName()));
    }

    @Override
    public void addUser(User user) {
        users.add(user);
        createLog(String.format(USER_ADDED_TO_TEAM,user.getUsername(),getName()));
    }

    @Override
    public void addToLog(HistoryLog log) {
        history.add(log);
    }

    @Override
    public void createLog(String message) {
        HistoryLog newLog = new HistoryLogImpl(message);
        addToLog(newLog);
    }

    @Override
    public Board findBoardByName(String boardName) {
        List<Board> boards = getBoards();

        return findItemByName(boards, boardName, BOARD_TYPE);
    }

    @Override
    public List<HistoryLog> getHistory() {
        return new ArrayList<>(history);
    }

    @Override
    public String toString() {
        return String.format(
                TEAM_TO_STRING,
                name);
    }

    private void setName(String name) {
        ValidationHelpers.validateIntRange(name.length(), NAME_MIN_VALUE, NAME_MAX_VALUE, NAME_LENGTH_ERROR);
        this.name = name;
    }

    private <T extends Nameable> T findItemByName(List<T> items, String title, String type) {
        return items
                .stream()
                .filter(item -> item.getName().equalsIgnoreCase(title))
                .findFirst()
                .orElseThrow(() -> new ElementNotFoundException(
                        String.format(ELEMENT_NOT_FOUND_ERROR,
                                type, title)));
    }
}
