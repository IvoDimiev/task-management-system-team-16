package models;

import exceptions.ElementNotFoundException;
import models.contracts.Board;
import models.contracts.HistoryLog;
import models.contracts.Identifiable;
import models.tasks.contracts.*;
import utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

public class BoardImpl implements Board {
    public static final int NAME_MIN_VALUE = 5;
    public static final int NAME_MAX_VALUE = 10;
    private static final String NAME_LENGTH_ERROR = String.format(
            "Board name should be between %d and %d characters!",
            NAME_MIN_VALUE, NAME_MAX_VALUE);
    public final static String BOARD_TO_STRING = "Board: %s";
    public final static String BOARD_CREATED = "Board with name: '%s' was created!";
    public final static String TASK_TYPE = "tasks";
    public final static String BUG_TYPE = "bug";
    public final static String FEEDBACK_TYPE = "feedbacks";
    public final static String STORY_TYPE = "stories";
    public final static String ASSIGNABLE_TYPE = "Assignable";
    public final static String STORY_CREATED = "Story with id: %d was created in Board: '%s' !";
    public final static String BUG_CREATED = "Bug with id: %d  was created in Board: '%s'!";
    public final static String FEEDBACK_CREATED = "Feedback with id: %d  was created in Board: '%s'!";
    public final static String NOT_FOUND_ERROR_MSG = "No %s with id %d";

    private String name;
    private final List<HistoryLog> history;
    private final List<Feedback> feedbacks;
    private final List<Story> stories;
    private final List<Bug> bugs;

    public BoardImpl(String name) {
        history = new ArrayList<>();
        feedbacks = new ArrayList<>();
        stories = new ArrayList<>();
        bugs = new ArrayList<>();
        setName(name);
        createLog(String.format(BOARD_CREATED, getName()));
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Task> getTasks() {
        List<Task> tasks = new ArrayList<>();
        tasks.addAll(feedbacks);
        tasks.addAll(bugs);
        tasks.addAll(stories);

        return new ArrayList<>(tasks);
    }

    @Override
    public List<Assignable> getAssignable() {
        List<Assignable> list = new ArrayList<>();
        list.addAll(bugs);
        list.addAll(stories);

        return list;
    }

    @Override
    public List<Prioritisable> getPrioritisable() {
        List<Prioritisable> list = new ArrayList<>();
        list.addAll(bugs);
        list.addAll(stories);

        return list;
    }

    @Override
    public List<HistoryLog> getHistory() {
        return new ArrayList<>(history);
    }


    @Override
    public List<Story> getStories() {
        return new ArrayList<>(stories);
    }

    @Override
    public List<Feedback> getFeedback() {
        return new ArrayList<>(feedbacks);
    }

    @Override
    public List<Bug> getBugs() {
        return new ArrayList<>(bugs);
    }

    @Override
    public Task findTaskById(int id) {
        return findItemById(getTasks(), id, TASK_TYPE);
    }

    @Override
    public Story findStoryById(int id) {
        return findItemById(stories, id, STORY_TYPE);
    }

    @Override
    public Feedback findFeedbackById(int id) {
        return findItemById(feedbacks, id, FEEDBACK_TYPE);
    }

    @Override
    public Bug findBugById(int id) {
        return findItemById(bugs, id, BUG_TYPE);
    }

    @Override
    public Assignable findAssignableById(int id) {
        List<Assignable> getAssignables = getAssignable();

        return findItemById(getAssignables, id, ASSIGNABLE_TYPE);
    }

    @Override
    public void addStory(Story story) {
        stories.add(story);
        createLog(String.format(STORY_CREATED, story.getId(), getName()));
    }

    @Override
    public void addFeedback(Feedback feedback) {
        feedbacks.add(feedback);
        createLog(String.format(FEEDBACK_CREATED, feedback.getId(), getName()));
    }

    @Override
    public void addBug(Bug bug) {
        bugs.add(bug);
        createLog(String.format(BUG_CREATED, bug.getId(), getName()));
    }

    @Override
    public void addToLog(HistoryLog log) {
        history.add(log);
    }

    @Override
    public void createLog(String message) {
        HistoryLog newLog = new HistoryLogImpl(message);
        addToLog(newLog);
    }

    @Override
    public String toString() {
        return String.format(
                BOARD_TO_STRING,
                name);
    }

    public <T extends Identifiable> T findItemById(List<T> items, int id, String type) {
        return items
                .stream()
                .filter(item -> item.getId() == id)
                .findFirst()
                .orElseThrow(() -> new ElementNotFoundException(
                        String.format(NOT_FOUND_ERROR_MSG,
                                type, id)));
    }

    private void setName(String name) {
        ValidationHelpers.validateIntRange(name.length(), NAME_MIN_VALUE, NAME_MAX_VALUE, NAME_LENGTH_ERROR);
        this.name = name;
    }

}

