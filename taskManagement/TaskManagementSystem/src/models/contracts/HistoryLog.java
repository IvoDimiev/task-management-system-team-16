package models.contracts;

public interface HistoryLog {
    String getDescription();
}
