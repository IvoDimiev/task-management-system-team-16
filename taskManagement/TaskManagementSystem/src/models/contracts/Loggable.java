package models.contracts;

import java.util.List;

public interface Loggable {
    void addToLog(HistoryLog log);

    void createLog(String message);

    List<HistoryLog> getHistory();
}
