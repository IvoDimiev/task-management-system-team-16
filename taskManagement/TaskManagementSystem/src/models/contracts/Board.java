package models.contracts;

import models.tasks.contracts.*;

import java.util.List;

public interface Board extends Loggable,Nameable {
    void addStory(Story story);

    void addFeedback(Feedback feedback);

    void addBug(Bug bug);

    List<Story> getStories();

    List<Feedback> getFeedback();

    List<Bug> getBugs();

    List<Task> getTasks();

    List<Assignable> getAssignable();

    List<Prioritisable>getPrioritisable();

    Task findTaskById(int id);

    Assignable findAssignableById(int id);

    Story findStoryById(int id);

    Feedback findFeedbackById(int id);

    Bug findBugById(int id);

    <T extends Identifiable> T findItemById(List<T> items, int id , String type);
}
