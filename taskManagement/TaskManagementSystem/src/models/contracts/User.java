package models.contracts;

public interface User extends Loggable,Identifiable,Nameable{
    String getUsername();

    String getPassword();

    void changePassword(String password);

    boolean isAdmin();
}
