package models.contracts;

import java.util.List;

public interface Team extends Loggable, Nameable {
    List<Board> getBoards();

    void addBoard(Board board);

    void addUser(User user);

    List<User> getUsers();

    Board findBoardByName(String boardName);

    String getName();
}
