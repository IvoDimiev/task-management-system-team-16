package models.contracts;

public interface Comment {
     String getContent();

     User getAuthor();
}
