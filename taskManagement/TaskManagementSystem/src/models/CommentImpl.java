package models;

import models.contracts.Comment;
import models.contracts.User;
import utils.ValidationHelpers;

public class CommentImpl implements Comment {
    public static final int CONTENT_MIN_VALUE = 10;
    public static final int CONTENT_MAX_VALUE = 500;
    private static final String CONTENT_LENGTH_ERROR = String.format(
            "The content of the comment should be between %d and %d characters!",
            CONTENT_MIN_VALUE, CONTENT_MAX_VALUE);
    public static final String COMMENT_TO_STRING = "%n----------%n" +
            "Content: '%s'%n" +
            "Author: %s%n" +
            "----------%n";

    private User author;
    private String content;

    public CommentImpl(User author, String content) {
        setAuthor(author);
        setContent(content);
    }

    @Override
    public User getAuthor() {
        return author;
    }

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public String toString() {
        return String.format(COMMENT_TO_STRING, getContent(), getAuthor());
    }

    private void setContent(String content) {
        ValidationHelpers.validateIntRange(content.length(), CONTENT_MIN_VALUE, CONTENT_MAX_VALUE, CONTENT_LENGTH_ERROR);
        this.content = content;
    }

    private void setAuthor(User author) {
        this.author = author;
    }
}
