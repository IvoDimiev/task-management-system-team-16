package core;

import core.Contracts.TaskManagementRepository;
import exceptions.ElementNotFoundException;
import models.BoardImpl;
import models.CommentImpl;
import models.TeamImpl;
import models.UserImpl;
import models.contracts.*;
import models.tasks.BugImpl;
import models.tasks.FeedbackImpl;
import models.tasks.StoryImpl;
import models.tasks.contracts.*;
import models.tasks.enums.Priority;
import models.tasks.enums.Severity;
import models.tasks.enums.Size;
import models.tasks.enums.UserRole;

import java.util.ArrayList;
import java.util.List;

public class TaskManagementRepositoryImpl implements TaskManagementRepository {
    private final List<User> users;
    private final List<Team> teams;
    private User loggedUser;
    private int id;
    private int taskId;


    public TaskManagementRepositoryImpl() {
        users = new ArrayList<>();
        teams = new ArrayList<>();
        id = 0;
        taskId = 0;
    }

    @Override
    public User findUserByUsername(String username) {
        try {
            return findItemByName(users, username, "user");
        } catch (ElementNotFoundException e) {
            throw new ElementNotFoundException("Wrong username!");
        }
    }

    @Override
    public Team findTeamByName(String name) {
        return findItemByName(teams, name, "team");
    }


    @Override
    public User createUser(String username, String password, UserRole role) {
        return new UserImpl(++id, username, password, role);
    }

    @Override
    public Team createTeam(List<User> users, String name) {
        return new TeamImpl(users, name);
    }

    @Override
    public BoardImpl createBoard(String name) {
        return new BoardImpl(name);
    }

    @Override
    public Bug createBug(String title, String description, Priority priority, Severity severity,
                         List<String> steps, Board board) {
        Bug bug = new BugImpl(++taskId, title, description, priority, severity, steps);
        board.addBug(bug);

        return bug;
    }

    @Override
    public Feedback createFeedback(String title, String description, int rating, Board board) {
        Feedback feedback = new FeedbackImpl(++taskId, title, description, rating);
        board.addFeedback(feedback);

        return feedback;
    }

    @Override
    public Story createStory(String title, String description, Priority priority, Size size, Board board) {
        Story story = new StoryImpl(++taskId, title, description, priority, size);
        board.addStory(story);

        return story;
    }

    @Override
    public Comment createComment(User author, String content) {
        return new CommentImpl(author, content);
    }

    @Override
    public void addTeam(Team team) {
        if (!teams.contains(team)) {
            teams.add(team);
        }
    }

    @Override
    public void addUser(User user) {
        if (!users.contains(user)) {
            users.add(user);
        }
    }

    @Override
    public void login(User user) {
        loggedUser = user;
    }

    @Override
    public List<Team> getTeams() {
        return new ArrayList<>(teams);
    }

    @Override
    public List<User> getUsers() {
        return new ArrayList<>(users);
    }

    @Override
    public void logout() {
        loggedUser = null;
    }

    @Override
    public boolean hasLoggedInUser() {
        return loggedUser != null;
    }

    @Override
    public User getLoggedInUser() {
        return loggedUser;
    }

    @Override
    public Board getCurrentBoard(List<String> subList) {
        String teamName = subList.get(0);
        String boardName = subList.get(1);
        Team team = findTeamByName(teamName);
        return team.findBoardByName(boardName);
    }

    private <T extends Nameable> T findItemByName(List<T> items, String title, String type) {
        return items
                .stream()
                .filter(item -> item.getName().equalsIgnoreCase(title))
                .findFirst()
                .orElseThrow(() -> new ElementNotFoundException(
                        String.format("No %s with title %s",
                                type, title)));
    }
}
