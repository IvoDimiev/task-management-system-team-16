package core;

import core.Contracts.OperationFactory;
import core.Contracts.TaskManagementRepository;
import operations.addOperations.AddCommentToATaskOperation;
import operations.assignOperations.AssignTaskToUserOperation;
import operations.assignOperations.UnassignTaskToUserOperation;
import operations.contracts.Operation;
import operations.createOperations.*;
import operations.enums.OperationType;
import operations.listingOperations.*;
import operations.showOperations.*;
import operations.changeOperations.*;
import operations.userOperations.*;
import utils.ParsingHelpers;

public class OperationFactoryImpl implements OperationFactory {
    @Override
    public Operation createOperationFromOperationName(String operationTypeAsString, TaskManagementRepository taskManagementRepository) {
        OperationType operationType = ParsingHelpers.tryParseEnum(operationTypeAsString, OperationType.class);
        switch (operationType) {
            case REGISTERNEWUSER:
                return new RegisterNewUserOperation(taskManagementRepository);
            case LOGIN:
                return new LogInOperation(taskManagementRepository);
            case LOGOUT:
                return new LogOutOperation(taskManagementRepository);
            case CHANGEPASSWORD:
                return new ChangePasswordOperation(taskManagementRepository);
            case INVITEUSER:
                return new InviteUserOperation(taskManagementRepository);
            case SHOWALLTEAMS:
                return new ShowAllTeamsOperation(taskManagementRepository);
            case SHOWALLTEAMMEMBERS:
                return new ShowAllTeamMembersOperation(taskManagementRepository);
            case SHOWALLTEAMBOARDS:
                return new ShowAllTeamBoardsOperation(taskManagementRepository);
            case SHOWALLUSERS:
                return new ShowAllUsersOperation(taskManagementRepository);
            case SHOWUSERSACTIVITY:
                return new ShowUserSActivityOperation(taskManagementRepository);
            case SHOWTEAMSACTIVITY:
                return new ShowTeamSActivityOperation(taskManagementRepository);
            case SHOWBOARDSACTIVITY:
                return new ShowBoardSActivityOperation(taskManagementRepository);
            case CREATETEAM:
                return new CreateTeamOperation(taskManagementRepository);
            case CREATEBOARD:
                return new CreateBoardOperation(taskManagementRepository);
            case CREATEBUG:
                return new CreateBugOperation(taskManagementRepository);
            case CREATESTORY:
                return new CreateStoryOperation(taskManagementRepository);
            case CREATEFEEDBACK:
                return new CreateFeedbackOperation(taskManagementRepository);
            case ASSIGNTASKTOUSER:
                return new AssignTaskToUserOperation(taskManagementRepository);
            case UNASSIGNTASKTOUSER:
                return new UnassignTaskToUserOperation(taskManagementRepository);
            case ADDCOMMENTTOATASK:
                return new AddCommentToATaskOperation(taskManagementRepository);
            case CHANGEBUGPRIORITY:
                return new ChangeBugPriorityOperation(taskManagementRepository);
            case CHANGEBUGSEVERITY:
                return new ChangeBugSeverityOperation(taskManagementRepository);
            case CHANGEBUGSTATUS:
                return new ChangeBugStatusOperation(taskManagementRepository);
            case CHANGEFEEDBACKRATING:
                return new ChangeFeedbackRatingOperation(taskManagementRepository);
            case CHANGEFEEDBACKSTATUS:
                return new ChangeFeedbackStatusOperation(taskManagementRepository);
            case CHANGESTORYPRIORITY:
                return new ChangeStoryPriorityOperation(taskManagementRepository);
            case CHANGESTORYSIZE:
                return new ChangeStorySizeOperation(taskManagementRepository);
            case CHANGESTORYSTATUS:
                return new ChangeStoryStatusOperation(taskManagementRepository);
            case SHOWTASKACTIVITY:
                return new ShowTaskActivityOperation(taskManagementRepository);
            case LISTALL:
                return new ListAllTasksOperation(taskManagementRepository);
            case LISTBUGS:
                return new ListBugsOperation(taskManagementRepository);
            case LISTSTORIES:
                return new ListStoriesOperation(taskManagementRepository);
            case LISTFEEDBACKS:
                return new ListFeedbacksOperation(taskManagementRepository);
            case FILTERBUGBYSEVERITY:
                return new FilterBugBySeverityOperation(taskManagementRepository);
            case FILTERALLBYTITLE:
                return new FilterAllByTitleOperation(taskManagementRepository);
            case FILTERBUGBYSTATUS:
                return new FilterBugByStatusOperation(taskManagementRepository);
            case FILTERFEEDBACKBYSTATUS:
                return new FilterFeedbackByStatusOperation(taskManagementRepository);
            case FILTERBYASSIGNEE:
                return new FilterByAssigneeOperation(taskManagementRepository);
            case FILTERSTORYBYSTATUS:
                return new FilterStoryByStatusOperation(taskManagementRepository);
            case SORTALLBYTITLE:
                return new SortAllByTitleOperation(taskManagementRepository);
            case SORTBUGBYSEVERITY:
                return new SortBugBySeverityOperation(taskManagementRepository);
            case SORTBUGBYTITLE:
                return new SortBugByTitleOperation(taskManagementRepository);
            case SORTFEEDBACKBYTITLE:
                return new SortFeedbackByTitleOperation(taskManagementRepository);
            case SORTFEEDBACKBYRATING:
                return new SortFeedbackByRatingOperation(taskManagementRepository);
            case SORTSTORYBYTITLE:
                return new SortStoryByTitleOperation(taskManagementRepository);
            case SORTBYPRIORITY:
                return new SortByPriorityOperation(taskManagementRepository);
            case SORTSTORYBYSIZE:
                return new SortStoryBySizeOperation(taskManagementRepository);
            case FILTERSTORYBYSTATUSANDASSIGNEE:
                return new FilterStoryByStatusAndAssignee(taskManagementRepository);
            case FILTERBUGBYSTATUSANDASSIGNEE:
                return new FilterBugByStatusAndAssignee(taskManagementRepository);
            default:
                throw new IllegalArgumentException("Command not available in the operation factory");
        }
    }

}
