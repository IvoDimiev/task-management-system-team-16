package core.Contracts;


import models.contracts.*;
import models.tasks.contracts.*;
import models.tasks.enums.Priority;
import models.tasks.enums.Severity;
import models.tasks.enums.Size;
import models.tasks.enums.UserRole;

import java.util.List;

public interface TaskManagementRepository {
    List<User> getUsers();

    User findUserByUsername(String username);

    Board getCurrentBoard(List<String> subList);

    User createUser(String username, String password, UserRole role);

    Team createTeam(List<User> users, String name);

    Board createBoard(String name);

    Task createBug(String title, String description, Priority priority, Severity severity,
                   List<String> steps, Board board);

    Comment createComment(User author, String content);

    Task createFeedback(String title, String description, int rating, Board board);

    Task createStory(String title, String description, Priority priority, Size size, Board board);

    void addUser(User user);

    void login(User user);

    void logout();

    boolean hasLoggedInUser();

    User getLoggedInUser();

    List<Team> getTeams();

    void addTeam(Team team);

    Team findTeamByName(String name);


}
