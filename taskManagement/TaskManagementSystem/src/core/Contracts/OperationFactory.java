package core.Contracts;

import operations.contracts.Operation;

public interface OperationFactory {
    Operation createOperationFromOperationName(String operationTypeAsString, TaskManagementRepository taskManagementRepository);
}
