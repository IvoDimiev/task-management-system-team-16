package utils;

import models.BoardImpl;
import models.TeamImpl;
import models.UserImpl;
import models.contracts.Board;
import models.contracts.Team;
import models.contracts.User;
import models.tasks.BugImpl;
import models.tasks.FeedbackImpl;
import models.tasks.StoryImpl;
import models.tasks.contracts.Bug;
import models.tasks.contracts.Feedback;
import models.tasks.contracts.Story;
import models.tasks.enums.UserRole;

import java.util.ArrayList;
import java.util.List;

import static utils.TestData.Board.VALID_BOARD_NAME;
import static utils.TestData.Bug.*;
import static utils.TestData.Team.VALID_TEAM_NAME;
import static utils.TestData.User.VALID_PASSWORD;
import static utils.TestData.User.VALID_USERNAME;
import static utils.TestData.Story.*;
import static utils.TestData.Feedback.*;

public class Factory {
    public static Team createTestTeam(User user) {
        List<User> users = new ArrayList<>();
        users.add(user);

        return new TeamImpl(users, VALID_TEAM_NAME);
    }

    public static Board createTestBoard() {
        return new BoardImpl(VALID_BOARD_NAME);
    }

    public static User createNormalUser() {
        return new UserImpl(1, VALID_USERNAME, VALID_PASSWORD, UserRole.DEVELOPER);
    }

    public static Bug createNormalBug(){
        return new BugImpl(10,VALID_BUG_NAME,VALID_BUG_DESCRIPTION,VALID_BUG_PRIORITY,VALID_SEVERITY,VALID_STEPS);
    }

    public static Story createNormalStory(){
        return new StoryImpl(11,VALID_STORY_NAME,VALID_STORY_DESCRIPTION,VALID_STORY_PRIORITY,VALID_SIZE);
    }

    public static Feedback createNormalFeedback(){
        return new FeedbackImpl(12,VALID_FEEDBACK_NAME,VALID_FEEDBACK_DESCRIPTION,VALID_RATING);
    }

    public static User createUser(UserRole userRole) {
        return new UserImpl(1, VALID_USERNAME, VALID_PASSWORD, userRole);
    }


}
