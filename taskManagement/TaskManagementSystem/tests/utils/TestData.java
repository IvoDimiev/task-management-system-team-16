package utils;


import models.tasks.FeedbackImpl;
import models.tasks.TaskBaseImpl;
import models.tasks.contracts.Statusable;
import models.tasks.enums.*;

import java.util.ArrayList;
import java.util.List;

import static models.TeamImpl.NAME_MIN_VALUE;
import static models.UserImpl.PASS_MAX;
import static models.UserImpl.USERNAME_LEN_MIN;

public class TestData {
/*    public static class Comment {
        public static final String VALID_CONTENT = TestUtilities.initializeStringWithSize(CONTENT_LEN_MIN + 1);
    }*/

    public static class User {
        public static final String VALID_USERNAME = TestUtilities.initializeStringWithSize(USERNAME_LEN_MIN + 1);
        public static final String VALID_PASSWORD = TestUtilities.initializeStringWithSize(PASS_MAX - 1);
        public static final String NEW_VALID_PASSWORD = "NEW_PASSWORD";
    }

    public static class Team {
        public static final String VALID_TEAM_NAME = TestUtilities.initializeStringWithSize(NAME_MIN_VALUE + 1);
    }
    public static class Board {
        public static final String VALID_BOARD_NAME = TestUtilities.initializeStringWithSize(NAME_MIN_VALUE + 1);
    }
    public static class Bug {
        public static final String VALID_BUG_NAME = TestUtilities.initializeStringWithSize(TaskBaseImpl.NAME_MIN_VALUE + 1);
        public static final String VALID_BUG_DESCRIPTION = TestUtilities.initializeStringWithSize(TaskBaseImpl.DESCRIPTION_MIN_VALUE+1);
        public static final Priority VALID_BUG_PRIORITY = Priority.LOW;
        public static final Priority NEW_VALID_BUG_PRIORITY = Priority.MEDIUM;
        public static final Severity VALID_SEVERITY=Severity.MINOR;
        public static final Severity NEW_VALID_SEVERITY=Severity.MAJOR;
        public static final List<String> VALID_STEPS = new ArrayList<>();
        public static final Statusable VALID_STATUS = BugStatus.ACTIVE;
        public static final Statusable NEW_VALID_STATUS=BugStatus.FIXED;

    }
    public static class Story{
        public static final String VALID_STORY_NAME = TestUtilities.initializeStringWithSize(TaskBaseImpl.NAME_MIN_VALUE + 1);
        public static final String VALID_STORY_DESCRIPTION = TestUtilities.initializeStringWithSize(TaskBaseImpl.DESCRIPTION_MIN_VALUE+1);
        public static final Priority VALID_STORY_PRIORITY = Priority.LOW;
        public static final Priority NEW_VALID_STORY_PRIORITY = Priority.MEDIUM;
        public static final Size VALID_SIZE= Size.MEDIUM;
        public static final Size NEW_VALID_SIZE= Size.SMALL;
        public static final Statusable VALID_STATUS = StoryStatus.NOT_DONE;
        public static final Statusable NEW_VALID_STATUS=StoryStatus.DONE;

    }
    public static class Feedback{
        public static final String VALID_FEEDBACK_NAME = TestUtilities.initializeStringWithSize(TaskBaseImpl.NAME_MIN_VALUE + 1);
        public static final String VALID_FEEDBACK_DESCRIPTION = TestUtilities.initializeStringWithSize(TaskBaseImpl.DESCRIPTION_MIN_VALUE+1);
        public static final int VALID_RATING = FeedbackImpl.RATING_MIN_VALUE+1;
        public static final int NEW_VALID_RATING = FeedbackImpl.RATING_MIN_VALUE+5;
        public static final Statusable VALID_STATUS = FeedbackStatus.NEW;
        public static final Statusable NEW_VALID_FEEDBACK_STATUS = FeedbackStatus.SCHEDULED;
    }
}