package utilsTests;

import exceptions.InvalidUserInputException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static utils.TestUtilities.initializeListWithSize;
import static utils.ValidationHelpers.validateArgumentsCount;

public class ValidationHelpers_Tests {

    @Test
    void validateArgumentsCount_shouldNotThrowException_when_argumentsHaveCorrectCount() {
        Assertions.assertDoesNotThrow(() -> validateArgumentsCount(initializeListWithSize(2), 2));
    }

    @Test
    void validateArgumentsCount_shouldThrowException_when_argumentsDoesntHaveCorrectCount() {
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> validateArgumentsCount(initializeListWithSize(2), 6));
    }


}
