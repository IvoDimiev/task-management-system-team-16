package core;

import core.Contracts.OperationFactory;
import core.Contracts.TaskManagementRepository;
import models.contracts.Board;
import models.contracts.Team;
import models.contracts.User;
import operations.contracts.Operation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.List;
import static utils.Factory.createNormalUser;
import static utils.Factory.createTestTeam;
import static utils.TestData.Team.VALID_TEAM_NAME;
import static utils.TestData.Board.VALID_BOARD_NAME;
import static utils.Factory.createTestBoard;


public class Command_Tests {
    private OperationFactory operationFactory;
    private TaskManagementRepository taskManagementRepository;
    private User user;
    private Team team;
    private Board board;

    @BeforeEach
    public void before() {
        operationFactory = new OperationFactoryImpl();
        taskManagementRepository = new TaskManagementRepositoryImpl();
        user = createNormalUser();
        taskManagementRepository.addUser(user);
        taskManagementRepository.login(user);
        team = createTestTeam(user);
        board = createTestBoard();
    }

    @Test
    public void createTeam_ShouldCreate_WhenInputIsValid() {
        // Act
        Operation createTeam = operationFactory.createOperationFromOperationName("CREATETEAM", taskManagementRepository);
        Team testTeam = createTestTeam(user);
        List<String> params = List.of(VALID_TEAM_NAME );

        // Arrange
        createTeam.execute(params);

        //Assert
        Team toCompare = taskManagementRepository.getTeams().get(0);
        Assertions.assertEquals(testTeam.getName(), toCompare.getName());
        Assertions.assertEquals(testTeam.getUsers().size(), toCompare.getUsers().size());

    }
    @Test
    public void createBoard_ShouldCreate_WhenInputIsValid() {
        // Act
        taskManagementRepository.addTeam(team);

        Operation createBoard = operationFactory.createOperationFromOperationName("CREATEBOARD", taskManagementRepository);

        Board testBoard = createTestBoard();
        List<String> params = List.of(VALID_BOARD_NAME,team.getName());

        // Arrange
        createBoard.execute(params);

        //Assert
        Board toCompare = team.getBoards().get(0);
        Assertions.assertEquals(testBoard.getName(), toCompare.getName());

    }
}
