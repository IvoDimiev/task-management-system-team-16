package operations;

import core.Contracts.TaskManagementRepository;
import core.TaskManagementRepositoryImpl;
import exceptions.ElementNotFoundException;
import exceptions.InvalidUserInputException;
import models.contracts.Board;
import models.contracts.Team;
import models.contracts.User;
import models.tasks.contracts.Story;
import operations.contracts.Operation;
import operations.listingOperations.ListAllTasksOperation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static utils.Factory.*;
import static utils.TestUtilities.initializeListWithSize;

public class ListAll_Tests {
    private final int EXPECTED_NUMBER_OF_PARAMETERS = 2;
    private Operation operation;
    private TaskManagementRepository taskManagementRepository;
    private User user;
    private Team testTeam;
    private Board testBoard;

    @BeforeEach
    public void before() {
        taskManagementRepository = new TaskManagementRepositoryImpl();
        operation = new ListAllTasksOperation(taskManagementRepository);
        user = createNormalUser();
        taskManagementRepository.addUser(user);
        taskManagementRepository.login(user);
        testTeam = createTestTeam(user);
        testBoard = createTestBoard();
        taskManagementRepository.addTeam(testTeam);
        testTeam.addBoard(testBoard);

    }
    @ParameterizedTest(name = "with arguments count: {0}")
    @ValueSource(ints = {EXPECTED_NUMBER_OF_PARAMETERS - 1, EXPECTED_NUMBER_OF_PARAMETERS + 1})
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected(int argumentsCount) {
        List<String> arguments = initializeListWithSize(argumentsCount);

        Assertions.assertThrows(InvalidUserInputException.class, () -> operation.execute(arguments));
    }

    @Test
    public void execute_should_ListAll_when_inputValid() {
        // Arrange
        Story story = createNormalStory();
        testBoard.addStory(story);
        List<String> parameters = List.of(testTeam.getName(),testBoard.getName());


        Assertions.assertAll(
                () -> Assertions.assertDoesNotThrow(() -> operation.execute(parameters)),
                () -> Assertions.assertEquals(story.toString(),operation.execute(parameters))
        );
    }
    @Test
    public void execute_Should_Return_String_whenListIsEmpty(){
        List<String> parameters = List.of(testTeam.getName(),testBoard.getName());
       Assertions.assertThrows(ElementNotFoundException.class,() -> operation.execute(parameters));

    }

}
