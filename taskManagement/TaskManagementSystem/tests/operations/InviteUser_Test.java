package operations;

import core.Contracts.TaskManagementRepository;
import core.TaskManagementRepositoryImpl;
import exceptions.InvalidUserInputException;
import models.contracts.Team;
import models.contracts.User;
import operations.contracts.Operation;
import operations.userOperations.InviteUserOperation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static utils.Factory.createNormalUser;
import static utils.Factory.createTestTeam;
import static utils.TestData.Team.VALID_TEAM_NAME;
import static utils.TestData.User.VALID_PASSWORD;
import static utils.TestUtilities.initializeListWithSize;

public class InviteUser_Test {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;
    private static final String NEW_USERNAME = "username56";

    private Operation operation;
    private TaskManagementRepository taskManagementRepository;
    private User user;
    private Team team;

    @BeforeEach
    public void before() {
        taskManagementRepository = new TaskManagementRepositoryImpl();
        operation = new InviteUserOperation(taskManagementRepository);
        user = createNormalUser();
        team = createTestTeam(user);

        taskManagementRepository.addTeam(team);
        taskManagementRepository.addUser(user);
        taskManagementRepository.login(user);
        team.addUser(user);
    }


    @ParameterizedTest(name = "with arguments count: {3}")
    @ValueSource(ints = {EXPECTED_NUMBER_OF_ARGUMENTS - 1, EXPECTED_NUMBER_OF_ARGUMENTS + 1})
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected(int argumentsCount) {
        List<String> arguments = initializeListWithSize(argumentsCount);

        Assertions.assertThrows(InvalidUserInputException.class, () -> operation.execute(arguments));
    }

    @Test
    public void execute_should_inviteUser_when_inputIsValid() {
        List<String> arguments = List.of(VALID_TEAM_NAME, NEW_USERNAME, VALID_PASSWORD);

        Assertions.assertDoesNotThrow(() -> operation.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_userIsnotLoggedIn() {
        List<String> arguments = List.of(VALID_TEAM_NAME, NEW_USERNAME, VALID_PASSWORD);
        taskManagementRepository.logout();

        Assertions.assertThrows(IllegalArgumentException.class, () -> operation.execute(arguments));
    }
}
