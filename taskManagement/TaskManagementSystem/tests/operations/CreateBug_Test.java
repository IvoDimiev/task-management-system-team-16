package operations;

import core.Contracts.TaskManagementRepository;
import core.TaskManagementRepositoryImpl;
import exceptions.ElementNotFoundException;
import exceptions.InvalidUserInputException;
import models.contracts.Board;
import models.contracts.Team;
import models.contracts.User;
import models.tasks.enums.Priority;
import models.tasks.enums.Severity;
import operations.contracts.Operation;
import operations.createOperations.CreateBugOperation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static utils.Factory.*;
import static utils.TestData.Board.VALID_BOARD_NAME;
import static utils.TestData.Team.VALID_TEAM_NAME;
import static utils.TestUtilities.initializeListWithSize;

public class CreateBug_Test {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 7;
    public static final String STEPS = "1. Open the application; 2. Click \"Log In\"; 3. The application freezes!";
    public static final String DESCRIPTION = "This needs to be fixed quickly!";
    public static final String VALID_BUG_TITLE = "The program freezes when the Log In button.";
    private static final String INVALID_BOARD_NAME = "The program is alright";

    private Operation operation;
    private TaskManagementRepository taskManagementRepository;
    private User user;
    private Team team;
    private Board board;

    @BeforeEach
    public void before() {
        taskManagementRepository = new TaskManagementRepositoryImpl();
        operation = new CreateBugOperation(taskManagementRepository);
        user = createNormalUser();
        team = createTestTeam(user);
        board = createTestBoard();

        taskManagementRepository.addUser(user);
        taskManagementRepository.login(user);
        taskManagementRepository.addTeam(team);
        taskManagementRepository.addUser(user);
        taskManagementRepository.login(user);

        team.addUser(user);
        team.addBoard(board);
    }

    @ParameterizedTest(name = "with arguments count: {3}")
    @ValueSource(ints = {EXPECTED_NUMBER_OF_ARGUMENTS - 1, EXPECTED_NUMBER_OF_ARGUMENTS + 1})
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected(int argumentsCount) {
        List<String> arguments = initializeListWithSize(argumentsCount);

        Assertions.assertThrows(InvalidUserInputException.class, () -> operation.execute(arguments));
    }

    @Test
    public void execute_should_createBug_whenArgumentsValid() {
        List<String> arguments = List.of(VALID_TEAM_NAME, VALID_BOARD_NAME, VALID_BUG_TITLE, DESCRIPTION, Priority.MEDIUM.name(),
                Severity.CRITICAL.name(), STEPS);

        Assertions.assertDoesNotThrow(() -> operation.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_boardDoesNotExist() {
        List<String> arguments = List.of(VALID_TEAM_NAME, INVALID_BOARD_NAME, VALID_BUG_TITLE, DESCRIPTION, Priority.MEDIUM.name(),
                Severity.CRITICAL.name(), STEPS);

        Assertions.assertThrows(ElementNotFoundException.class, () -> operation.execute(arguments));
    }
}
