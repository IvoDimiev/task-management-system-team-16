package operations;

import core.Contracts.TaskManagementRepository;
import core.TaskManagementRepositoryImpl;
import models.contracts.User;
import operations.contracts.Operation;
import operations.showOperations.ShowAllUsersOperation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static utils.Factory.createNormalUser;

public class ShowAllUsersTest {
    private Operation operation;
    private TaskManagementRepository taskManagementRepository;
    private User user;

    @BeforeEach
    public void before() {
        taskManagementRepository = new TaskManagementRepositoryImpl();
        operation = new ShowAllUsersOperation(taskManagementRepository);
        user = createNormalUser();
        taskManagementRepository.addUser(user);
        taskManagementRepository.login(user);
    }

    @Test
    public void execute_should_printAllUsers_when_argumetnsValid() {
        Assertions.assertAll(
                () -> Assertions.assertDoesNotThrow(() -> operation.execute(new ArrayList<>())),
                () -> Assertions.assertEquals(1, taskManagementRepository.getUsers().size())
        );
    }
}
