package operations;

import core.Contracts.TaskManagementRepository;
import core.TaskManagementRepositoryImpl;
import exceptions.InvalidUserInputException;
import models.contracts.Board;
import models.contracts.Team;
import models.contracts.User;
import operations.contracts.Operation;
import operations.createOperations.CreateBoardOperation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static utils.Factory.*;
import static utils.TestData.Board.VALID_BOARD_NAME;
import static utils.TestUtilities.initializeListWithSize;

public class CreateBoard_Test {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private Operation operation;
    private TaskManagementRepository taskManagementRepository;
    private User user;
    private Team team;

    @BeforeEach
    public void before() {
        taskManagementRepository = new TaskManagementRepositoryImpl();
        operation = new CreateBoardOperation(taskManagementRepository);
        user = createNormalUser();
        taskManagementRepository.addUser(user);
        taskManagementRepository.login(user);
        team = createTestTeam(user);
        taskManagementRepository.addTeam(team);
        taskManagementRepository.addUser(user);
        taskManagementRepository.login(user);
        team.addUser(user);
    }

    @ParameterizedTest(name = "with arguments count: {2}")
    @ValueSource(ints = {EXPECTED_NUMBER_OF_ARGUMENTS - 1, EXPECTED_NUMBER_OF_ARGUMENTS + 1})
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected(int argumentsCount) {
        List<String> arguments = initializeListWithSize(argumentsCount);

        Assertions.assertThrows(InvalidUserInputException.class, () -> operation.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_boardWithSameNameExists() {
        Board board = createTestBoard();
        team.addBoard(board);
        List<String> arguments = List.of(VALID_BOARD_NAME, team.getName());

        Assertions.assertThrows(IllegalArgumentException.class, () -> operation.execute(arguments));
    }

    @Test
    public void execute_should_createBoard_when_argumentsAreValid() {
        List<String> arguments = List.of(VALID_BOARD_NAME, team.getName());
        operation.execute(arguments);
        Assertions.assertEquals(1, team.getBoards().size());
    }
}
