package operations;

import core.Contracts.TaskManagementRepository;
import core.TaskManagementRepositoryImpl;
import exceptions.ElementNotFoundException;
import exceptions.InvalidUserInputException;
import models.contracts.Board;
import models.contracts.Team;
import models.contracts.User;
import models.tasks.enums.UserRole;
import operations.contracts.Operation;
import operations.showOperations.ShowAllTeamBoardsOperation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static utils.Factory.*;
import static utils.TestData.Team.VALID_TEAM_NAME;
import static utils.TestUtilities.initializeListWithSize;

public class ShowAllTeamBoards_Test {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    private static final String NEW_BOARD_NAME = "board2";

    private Operation operation;
    private TaskManagementRepository taskManagementRepository;
    private User user;
    private Team team;
    private Board board;

    @BeforeEach
    public void before() {
        taskManagementRepository = new TaskManagementRepositoryImpl();
        operation = new ShowAllTeamBoardsOperation(taskManagementRepository);
        user = createUser(UserRole.ADMIN);
        team = createTestTeam(user);
        board = createTestBoard();

        taskManagementRepository.addTeam(team);
        taskManagementRepository.addUser(user);
        taskManagementRepository.login(user);
        team.addUser(user);
        team.addBoard(board);
    }

    @ParameterizedTest(name = "with arguments count: {1}")
    @ValueSource(ints = {EXPECTED_NUMBER_OF_ARGUMENTS - 1, EXPECTED_NUMBER_OF_ARGUMENTS + 1})
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected(int argumentsCount) {
        List<String> arguments = initializeListWithSize(argumentsCount);

        Assertions.assertThrows(InvalidUserInputException.class, () -> operation.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_boardDoesNotExist() {
        List<String> arguments = List.of(NEW_BOARD_NAME);

        Assertions.assertThrows(ElementNotFoundException.class, () -> operation.execute(arguments));
    }

    @Test
    public void execute_should_printAllTeamMembers_when_argumentsAreValid() {
        List<String> arguments = List.of(VALID_TEAM_NAME);

        Assertions.assertDoesNotThrow(() -> operation.execute(arguments));
    }
}
