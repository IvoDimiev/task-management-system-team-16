package operations;

import core.Contracts.TaskManagementRepository;
import core.TaskManagementRepositoryImpl;
import exceptions.InvalidUserInputException;
import models.contracts.User;
import models.tasks.enums.UserRole;
import operations.contracts.Operation;
import operations.userOperations.RegisterNewUserOperation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static utils.Factory.createUser;
import static utils.TestData.User.VALID_PASSWORD;
import static utils.TestData.User.VALID_USERNAME;
import static utils.TestUtilities.initializeListWithSize;

public class RegisterNewUser_Test {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private Operation operation;
    private TaskManagementRepository taskManagementRepository;

    @BeforeEach
    public void before() {
        taskManagementRepository = new TaskManagementRepositoryImpl();
        operation = new RegisterNewUserOperation(taskManagementRepository);
    }

    @ParameterizedTest(name = "with arguments count: {2}")
    @ValueSource(ints = {EXPECTED_NUMBER_OF_ARGUMENTS - 1, EXPECTED_NUMBER_OF_ARGUMENTS + 1})
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected(int argumentsCount) {
        List<String> arguments = initializeListWithSize(argumentsCount);

        Assertions.assertThrows(InvalidUserInputException.class, () -> operation.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_userAlreadyLoggedIn() {
        User user = createUser(UserRole.ADMIN);
        taskManagementRepository.addUser(user);
        taskManagementRepository.login(user);
        List<String> arguments = List.of(VALID_USERNAME, VALID_PASSWORD);

        Assertions.assertThrows(IllegalArgumentException.class, () -> operation.execute(arguments));

    }

    @Test
    public void execute_should_loginUser_when_inputIsValid() {
        User user = createUser(UserRole.ADMIN);
        taskManagementRepository.addUser(user);
        taskManagementRepository.login(user);

        Assertions.assertNotNull(taskManagementRepository.getLoggedInUser());
    }
}
