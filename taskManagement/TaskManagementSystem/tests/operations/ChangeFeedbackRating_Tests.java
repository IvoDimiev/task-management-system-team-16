package operations;

import core.Contracts.TaskManagementRepository;
import core.TaskManagementRepositoryImpl;
import exceptions.InvalidUserInputException;
import models.contracts.Board;
import models.contracts.Team;
import models.contracts.User;
import models.tasks.contracts.Feedback;
import operations.changeOperations.ChangeFeedbackRatingOperation;
import operations.contracts.Operation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import utils.TestData;

import java.util.List;

import static operations.changeOperations.ChangeFeedbackRatingOperation.EXPECTED_NUMBER_OF_PARAMETERS;
import static utils.Factory.*;
import static utils.TestUtilities.initializeListWithSize;

public class ChangeFeedbackRating_Tests {
    private Operation operation;
    private TaskManagementRepository taskManagementRepository;
    private User user;
    private Team testTeam;
    private Board testBoard;

    @BeforeEach
    public void before() {
        taskManagementRepository = new TaskManagementRepositoryImpl();
        operation = new ChangeFeedbackRatingOperation(taskManagementRepository);
        user = createNormalUser();
        taskManagementRepository.addUser(user);
        taskManagementRepository.login(user);
         testTeam = createTestTeam(user);
         testBoard = createTestBoard();
         taskManagementRepository.addTeam(testTeam);
         testTeam.addBoard(testBoard);


    }

    @ParameterizedTest(name = "with arguments count: {0}")
    @ValueSource(ints = {EXPECTED_NUMBER_OF_PARAMETERS - 1, EXPECTED_NUMBER_OF_PARAMETERS + 1})
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected(int argumentsCount) {
        List<String> parameters = initializeListWithSize(argumentsCount);

        Assertions.assertThrows(InvalidUserInputException.class, () -> operation.execute(parameters));
    }
    @Test
    public void execute_should_throwException_when_id_Unparsable() {
        // Arrange, Act

        List<String> parameters = List.of(testTeam.getName(),testBoard.getName(),"id","50");

        // Arrange
        Assertions.assertThrows(IllegalArgumentException.class, () -> operation.execute(parameters));

    }
    @Test
    public void execute_should_throwException_when_Rating_Unparsable() {
        // Arrange, Act

        List<String> parameters = List.of(testTeam.getName(),testBoard.getName(),"3","fifity");

        // Arrange
        Assertions.assertThrows(IllegalArgumentException.class, () -> operation.execute(parameters));

    }

    @Test
    public void execute_should_ChangeFeedbackRating_when_inputValid() {
        // Arrange
        Feedback feedback = createNormalFeedback();
        testBoard.addFeedback(feedback);
        List<String> parameters = List.of(testTeam.getName(),testBoard.getName(),String.valueOf(feedback.getId()),String.valueOf(TestData.Feedback.NEW_VALID_RATING));


        Assertions.assertAll(
               () -> Assertions.assertDoesNotThrow(() -> operation.execute(parameters)),
                () -> Assertions.assertEquals(feedback.getRating(),TestData.Feedback.NEW_VALID_RATING)
        );
    }

}
