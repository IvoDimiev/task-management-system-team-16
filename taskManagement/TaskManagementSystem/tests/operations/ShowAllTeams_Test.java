package operations;

import core.Contracts.TaskManagementRepository;
import core.TaskManagementRepositoryImpl;
import models.contracts.Team;
import models.contracts.User;
import operations.contracts.Operation;
import operations.showOperations.ShowAllTeamsOperation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static utils.Factory.createNormalUser;
import static utils.Factory.createTestTeam;

public class ShowAllTeams_Test {
    private Operation operation;
    private TaskManagementRepository taskManagementRepository;
    private User user;
    private Team team;

    @BeforeEach
    public void before() {
        taskManagementRepository = new TaskManagementRepositoryImpl();
        operation = new ShowAllTeamsOperation(taskManagementRepository);
        user = createNormalUser();
        team = createTestTeam(user);
        taskManagementRepository.addTeam(team);
        taskManagementRepository.addUser(user);
        taskManagementRepository.login(user);
    }


    @Test
    public void execute_should_printAllTeams_when_argumentsValid() {
        Assertions.assertAll(
                () -> Assertions.assertDoesNotThrow(() -> operation.execute(new ArrayList<>())),
                () -> Assertions.assertEquals(1, taskManagementRepository.getTeams().size())
        );
    }
}
