package operations;

import core.Contracts.TaskManagementRepository;
import core.TaskManagementRepositoryImpl;
import exceptions.InvalidUserInputException;
import models.contracts.User;
import operations.contracts.Operation;
import operations.userOperations.ChangePasswordOperation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static utils.Factory.createNormalUser;
import static utils.TestData.User.NEW_VALID_PASSWORD;
import static utils.TestData.User.VALID_PASSWORD;
import static utils.TestUtilities.initializeListWithSize;

public class ChangePassword_Test {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private Operation operation;
    private TaskManagementRepository taskManagementRepository;
    private User user;

    @BeforeEach
    public void before() {
        taskManagementRepository = new TaskManagementRepositoryImpl();
        operation = new ChangePasswordOperation(taskManagementRepository);
        user = createNormalUser();
        taskManagementRepository.addUser(user);
        taskManagementRepository.login(user);
    }

    @ParameterizedTest(name = "with arguments count: {2}")
    @ValueSource(ints = {EXPECTED_NUMBER_OF_ARGUMENTS - 1, EXPECTED_NUMBER_OF_ARGUMENTS + 1})
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected(int argumentsCount) {
        List<String> arguments = initializeListWithSize(argumentsCount);

        Assertions.assertThrows(InvalidUserInputException.class, () -> operation.execute(arguments));
    }

    @Test
    public void execute_should_changePassword_when_inputIsValid() {
        List<String> arguments = List.of(VALID_PASSWORD, NEW_VALID_PASSWORD);

        operation.execute(arguments);

        Assertions.assertEquals(NEW_VALID_PASSWORD, user.getPassword());
    }
}
