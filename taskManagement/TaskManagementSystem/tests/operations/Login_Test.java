package operations;

import core.Contracts.TaskManagementRepository;
import core.TaskManagementRepositoryImpl;
import exceptions.InvalidUserInputException;
import models.contracts.User;
import operations.userOperations.LogInOperation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static utils.Factory.createNormalUser;
import static utils.TestData.User.*;
import static utils.TestUtilities.initializeListWithSize;

public class Login_Test {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private BaseOperation operation;
    private TaskManagementRepository taskManagementRepository;
    private User user;

    @BeforeEach
    public void before() {
        taskManagementRepository = new TaskManagementRepositoryImpl();
        operation = new LogInOperation(taskManagementRepository);
        user = createNormalUser();
        taskManagementRepository.addUser(user);
    }

    @ParameterizedTest(name = "with arguments count: {0}")
    @ValueSource(ints = {EXPECTED_NUMBER_OF_ARGUMENTS - 1, EXPECTED_NUMBER_OF_ARGUMENTS + 1})
    public void execute_should_throwException_when_commandArgumentsCountDifferentThanExpected(int argumentsCount) {
        List<String> arguments = initializeListWithSize(argumentsCount);

        Assertions.assertThrows(InvalidUserInputException.class, () -> operation.executeOperation(arguments));
    }

    @Test
    public void  execute_should_loginUser_when_argumentsValid() {
        List<String> arguments = List.of(VALID_USERNAME, VALID_PASSWORD);
        Assertions.assertAll(
                () -> Assertions.assertDoesNotThrow(() -> operation.execute(arguments)),
                () -> Assertions.assertNotNull(taskManagementRepository.getLoggedInUser())
        );
    }

    @Test
    public void execute_should_throwException_whenUserAlreadyLoggedIn() {
        taskManagementRepository.login(user);
        List<String> arguments = List.of(VALID_USERNAME, VALID_PASSWORD);

        Assertions.assertThrows(IllegalArgumentException.class, () -> operation.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_passwordIsWrong() {
        List<String> arguments = List.of(VALID_USERNAME, "WRONG_PASSWORD");

        Assertions.assertThrows(IllegalArgumentException.class, () -> operation.execute(arguments));
    }

}
