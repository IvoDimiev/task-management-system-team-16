package operations;

import core.Contracts.TaskManagementRepository;
import core.TaskManagementRepositoryImpl;
import exceptions.InvalidUserInputException;
import models.contracts.User;
import operations.userOperations.LogOutOperation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.ArrayList;
import java.util.List;

import static utils.Factory.createNormalUser;
import static utils.TestUtilities.initializeListWithSize;

public class Logout_Test {
    public static final int EXPECTED_NUMBER_OF_PARAMETERS = 0;

    private BaseOperation operation;
    private TaskManagementRepository taskManagementRepository;
    private User user;

    @BeforeEach
    public void before() {
        taskManagementRepository = new TaskManagementRepositoryImpl();
        operation = new LogOutOperation(taskManagementRepository);
        user = createNormalUser();
        taskManagementRepository.addUser(user);
        taskManagementRepository.login(user);
    }

    @ParameterizedTest(name = "with arguments count: {0}")
    @ValueSource(ints = {EXPECTED_NUMBER_OF_PARAMETERS + 1})
    public void execute_should_throwException_when_commandArgumentsCountDifferentThanExpected(int argumentsCount) {
        List<String> arguments = initializeListWithSize(argumentsCount);

        Assertions.assertThrows(InvalidUserInputException.class, () -> operation.executeOperation(arguments));
    }

    @Test
    public void execute_should_logoutUser_when_inputIsValid() {
        operation.execute(new ArrayList<>());
        Assertions.assertNull(taskManagementRepository.getLoggedInUser());
    }
}
