package operations;

import core.Contracts.TaskManagementRepository;
import core.TaskManagementRepositoryImpl;
import exceptions.InvalidUserInputException;
import models.contracts.Board;
import models.contracts.Team;
import models.contracts.User;
import models.tasks.contracts.Bug;
import models.tasks.enums.BugStatus;
import operations.changeOperations.ChangeBugStatusOperation;
import operations.contracts.Operation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import utils.TestData;

import java.util.List;

import static operations.changeOperations.ChangeBugStatusOperation.EXPECTED_NUMBER_OF_PARAMETERS;
import static utils.Factory.*;
import static utils.TestUtilities.initializeListWithSize;

public class ChangeBugStatus_Tests {
    private Operation operation;
    private TaskManagementRepository taskManagementRepository;
    private User user;
    private Team testTeam;
    private Board testBoard;

    @BeforeEach
    public void before() {
        taskManagementRepository = new TaskManagementRepositoryImpl();
        operation = new ChangeBugStatusOperation(taskManagementRepository);
        user = createNormalUser();
        taskManagementRepository.addUser(user);
        taskManagementRepository.login(user);
        testTeam = createTestTeam(user);
        testBoard = createTestBoard();
        taskManagementRepository.addTeam(testTeam);
        testTeam.addBoard(testBoard);
    }
    @ParameterizedTest(name = "with arguments count: {0}")
    @ValueSource(ints = {EXPECTED_NUMBER_OF_PARAMETERS - 1, EXPECTED_NUMBER_OF_PARAMETERS + 1})
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected(int argumentsCount) {
        List<String> arguments = initializeListWithSize(argumentsCount);

        Assertions.assertThrows(InvalidUserInputException.class, () -> operation.execute(arguments));
    }
    @Test
    public void execute_should_throwException_when_id_Unparsable() {
        // Arrange, Act

        List<String> parameters = List.of(testTeam.getName(),testBoard.getName(),"id", BugStatus.ACTIVE.toString());

        // Arrange
        Assertions.assertThrows(IllegalArgumentException.class, () -> operation.execute(parameters));

    }
    @Test
    public void execute_should_throwException_when_Status_Unparsable() {
        // Arrange, Act

        List<String> parameters = List.of(testTeam.getName(),testBoard.getName(),"10","132322");

        // Arrange
        Assertions.assertThrows(IllegalArgumentException.class, () -> operation.execute(parameters));

    }

    @Test
    public void execute_should_ChangeStoryStatus_when_inputValid() {
        // Arrange
        Bug bug = createNormalBug();
        testBoard.addBug(bug);
        List<String> parameters = List.of(testTeam.getName(),testBoard.getName(),String.valueOf(bug.getId()),String.valueOf(TestData.Bug.NEW_VALID_STATUS));


        Assertions.assertAll(
                () -> Assertions.assertDoesNotThrow(() -> operation.execute(parameters)),
                () -> Assertions.assertEquals(bug.getStatus(),TestData.Bug.NEW_VALID_STATUS)
        );
    }
}
