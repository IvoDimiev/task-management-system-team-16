package operations;

import core.Contracts.TaskManagementRepository;
import core.TaskManagementRepositoryImpl;
import exceptions.ElementNotFoundException;
import models.contracts.Board;
import models.contracts.Team;
import models.contracts.User;
import models.tasks.contracts.Bug;
import operations.assignOperations.UnassignTaskToUserOperation;
import operations.contracts.Operation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static utils.Factory.*;
import static utils.TestUtilities.initializeListWithSize;

public class UnnasignTaskToUser_tests {

    private final int EXPECTED_NUMBER_OF_PARAMETERS = 4;
    private Operation operation;
    private TaskManagementRepository taskManagementRepository;
    private User user;
    private Team testTeam;
    private Board testBoard;
    private Bug bug;

    @BeforeEach
    public void before() {
        taskManagementRepository = new TaskManagementRepositoryImpl();
        operation = new UnassignTaskToUserOperation(taskManagementRepository);
        user = createNormalUser();
        taskManagementRepository.addUser(user);
        taskManagementRepository.login(user);
        testTeam = createTestTeam(user);
        testBoard = createTestBoard();
        bug = createNormalBug();
        testBoard.addBug(bug);
        taskManagementRepository.addTeam(testTeam);
        testTeam.addBoard(testBoard);
    }

    @ParameterizedTest(name = "with arguments count: {0}")
    @ValueSource(ints = {EXPECTED_NUMBER_OF_PARAMETERS - 1, EXPECTED_NUMBER_OF_PARAMETERS + 1})
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected(int argumentsCount) {
        List<String> arguments = initializeListWithSize(argumentsCount);

        Assertions.assertThrows(ElementNotFoundException.class, () -> operation.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_taskDoesNotExist() {
        List<String> parameters = List.of(testTeam.getName(),testBoard.getName(), "101", user.getUsername());

        Assertions.assertThrows(ElementNotFoundException.class, () -> operation.execute(parameters));
    }

    @Test
    public void execute_should_assignUser_when_inputIsValid() {
        List<String> parameters = List.of(testTeam.getName(),testBoard.getName(), Integer.toString(bug.getId()), user.getUsername());
        operation.execute(parameters);
        Assertions.assertEquals("Unassigned", bug.getAssignee().getUsername());
    }
}
