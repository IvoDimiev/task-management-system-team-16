package operations;

import core.Contracts.TaskManagementRepository;
import core.TaskManagementRepositoryImpl;
import exceptions.InvalidUserInputException;
import models.contracts.User;
import models.tasks.enums.UserRole;
import operations.userOperations.CreateNewUserOperation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static utils.Factory.createUser;
import static utils.TestData.User.VALID_PASSWORD;
import static utils.TestData.User.VALID_USERNAME;
import static utils.TestUtilities.initializeListWithSize;

public class CreateNewUser_Test {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;

    private CreateNewUserOperation operation;
    private TaskManagementRepository taskManagementRepository;

    @BeforeEach
    public void before() {
        taskManagementRepository = new TaskManagementRepositoryImpl();
        operation = new CreateNewUserOperation(taskManagementRepository);
    }

    @ParameterizedTest(name = "with arguments count: {0}")
    @ValueSource(ints = {EXPECTED_NUMBER_OF_ARGUMENTS - 1, EXPECTED_NUMBER_OF_ARGUMENTS + 1})
    public void execute_should_throwException_when_commandArgumentsCountDifferentThanExpected(int argumentsCount) {
        List<String> arguments = initializeListWithSize(argumentsCount);

        Assertions.assertThrows(InvalidUserInputException.class, () -> operation.executeOperation(arguments));
    }

    @Test
    public void execute_should_throwException_when_userWithSameUsernameExists() {
        User user = createUser(UserRole.ADMIN);
        taskManagementRepository.addUser(user);
        List<String> arguments = List.of(VALID_USERNAME, VALID_PASSWORD, "Admin");

        Assertions.assertThrows(IllegalArgumentException.class, () -> operation.executeOperation(arguments));
    }

    @Test
    public void execute_should_createUser_when_argumentsValid() {
        User user = createUser(UserRole.ADMIN);
        taskManagementRepository.addUser(user);

        Assertions.assertEquals(1, taskManagementRepository.getUsers().size());
    }
}
