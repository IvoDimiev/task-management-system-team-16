package operations;

import core.Contracts.TaskManagementRepository;
import core.TaskManagementRepositoryImpl;
import exceptions.InvalidUserInputException;
import models.contracts.User;
import operations.contracts.Operation;
import operations.showOperations.ShowTeamSActivityOperation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static operations.showOperations.ShowTeamSActivityOperation.EXPECTED_NUMBER_OF_PARAMETERS;
import static utils.Factory.createNormalUser;
import static utils.TestUtilities.initializeListWithSize;

public class ShowTeamSActivity_Tests {
    private Operation operation;
    private TaskManagementRepository taskManagementRepository;
    private User user;

    @BeforeEach
    public void before() {
        taskManagementRepository = new TaskManagementRepositoryImpl();
        operation = new ShowTeamSActivityOperation(taskManagementRepository);
        user = createNormalUser();
        taskManagementRepository.addUser(user);
        taskManagementRepository.login(user);
    }

    @ParameterizedTest(name = "with arguments count: {0}")
    @ValueSource(ints = {EXPECTED_NUMBER_OF_PARAMETERS - 1, EXPECTED_NUMBER_OF_PARAMETERS + 1})
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected(int argumentsCount) {
        List<String> arguments = initializeListWithSize(argumentsCount);

        Assertions.assertThrows(InvalidUserInputException.class, () -> operation.execute(arguments));
    }

}
