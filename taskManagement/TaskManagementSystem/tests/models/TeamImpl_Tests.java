package models;

import core.Contracts.TaskManagementRepository;
import core.TaskManagementRepositoryImpl;
import exceptions.ElementNotFoundException;
import exceptions.InvalidUserInputException;
import models.contracts.Board;
import models.contracts.HistoryLog;
import models.contracts.Team;
import models.contracts.User;
import models.tasks.enums.UserRole;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import utils.TestData;
import utils.TestUtilities;

import java.util.ArrayList;
import java.util.List;

import static utils.Factory.createNormalUser;
import static utils.Factory.createTestTeam;

public class TeamImpl_Tests {
    private TaskManagementRepository taskManagementRepository;
    private User user;

    @BeforeEach
    public void before() {
        user = createNormalUser();
        taskManagementRepository = new TaskManagementRepositoryImpl();
        taskManagementRepository.addUser(user);
        taskManagementRepository.login(user);
    }

    @ParameterizedTest(name = "with length {0}")
    @ValueSource(ints = {TeamImpl.NAME_MIN_VALUE - 1,TeamImpl.NAME_MAX_VALUE + 1})
    public void should_throwException_when_usernameLengthOutOfBounds(int testLength) {
        // Arrange, Act, Assert
        List<User> users = new ArrayList<>();
        users.add(user);
        Assertions.assertThrows(InvalidUserInputException.class, () ->
                new TeamImpl(users,
                        TestUtilities.initializeStringWithSize(testLength)));
    }

    @Test
    public void should_addEventToLog_when_InputIsValid(){
        List<User> users = new ArrayList<>();
        users.add(user);
        Team testTeam = new TeamImpl(users,"Manchester");

        Assertions.assertEquals(1, testTeam.getHistory().size());
    }
    @Test
    public void getHistory_should_returnCopy() {
        List<User> users = new ArrayList<>();
        users.add(user);
        Team testTeam = new TeamImpl(users,"Manchester");
        List<HistoryLog> logs = testTeam.getHistory();
        logs.add(new HistoryLogImpl("test log added"));

        Assertions.assertEquals(1, testTeam.getHistory().size());
    }
    @Test
    public void addUser_shouldAddUserToList() {
        Team team = createTestTeam(user);
        User secondUser = new UserImpl(0,"username","password", UserRole.ADMIN);
        int sizeBeforeAdd = team.getUsers().size();
        team.addUser(secondUser);

        Assertions.assertNotEquals(sizeBeforeAdd,team.getUsers().size());
    }

    @Test
    public void toString_Should_return_Correct_String_Format(){
        Team team = createTestTeam(user);
        Assertions.assertEquals(String.format(TeamImpl.TEAM_TO_STRING,TestData.Team.VALID_TEAM_NAME),team.toString());
    }

    @Test
    public void findItemByName_ShouldThrow_WhenBoardDoesNotExist() {
        Team team = createTestTeam(user);
        Board testBoard = new BoardImpl("TestBoard");
        String testBoardName= testBoard.getName();
        Assertions.assertThrows(ElementNotFoundException.class, () -> team.findBoardByName(testBoardName));
    }

}
