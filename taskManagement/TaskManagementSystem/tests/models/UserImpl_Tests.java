package models;

import core.Contracts.TaskManagementRepository;
import core.TaskManagementRepositoryImpl;
import exceptions.InvalidUserInputException;
import models.contracts.HistoryLog;
import models.contracts.User;
import models.tasks.enums.UserRole;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import utils.TestData;
import utils.TestUtilities;

import java.util.List;

import static utils.Factory.createNormalUser;

public class UserImpl_Tests {
    private TaskManagementRepository taskManagementRepository;
    private User user;

    @BeforeEach
    public void before() {
        user = createNormalUser();
        taskManagementRepository = new TaskManagementRepositoryImpl();
        taskManagementRepository.addUser(user);
        taskManagementRepository.login(user);
    }
    @Test
    public void User_ShouldImplementUserInterface() {
        Assertions.assertTrue(user instanceof User);
    }

    @ParameterizedTest(name = "with length {0}")
    @ValueSource(ints = {UserImpl.USERNAME_LEN_MIN - 1,UserImpl.USERNAME_LEN_MAX + 1})
    public void should_throwException_when_usernameLengthOutOfBounds(int testLength) {
        // Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () ->
                new UserImpl(0,
                        TestUtilities.initializeStringWithSize(testLength),
                        TestData.User.VALID_PASSWORD,
                        UserRole.DEVELOPER));
    }

    @ParameterizedTest(name = "with length {0}")
    @ValueSource(ints = {UserImpl.PASS_MIN - 1, UserImpl.PASS_MAX + 1})
    public void should_throwException_when_passwordLengthOutOfBounds(int testLength) {
        // Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () ->
                new UserImpl(0,
                        TestData.User.VALID_USERNAME,
                        TestUtilities.initializeStringWithSize(testLength),
                        UserRole.DEVELOPER));
    }

    @Test
    public void Constructor_ShouldThrow_WhenUsernameDoesNotMatchPattern() {
        //Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> new UserImpl(0,
                        "U$$ernam3",
                        "password",
                        UserRole.DEVELOPER));
    }

    @Test
    public void getHistory_should_returnCopy() {
        List<HistoryLog> logs = user.getHistory();

        logs.add(new HistoryLogImpl("test log added"));

        Assertions.assertEquals(1, user.getHistory().size());
    }

    @Test
    public void addToLog_should_add(){
        user.addToLog(new HistoryLogImpl("test log added"));

        Assertions.assertEquals(2, user.getHistory().size());
    }

    @Test
    public void isAdmin_should_return_false_when_role_isDeveloper(){
        boolean testValue = user.isAdmin();

        Assertions.assertFalse(testValue);
    }

    @Test
    public void isAdmin_should_return_true_when_role_isAdmin(){
        boolean testValue = (new UserImpl(0,"Admin","password",UserRole.ADMIN).isAdmin());

        Assertions.assertTrue(testValue);
    }

    @Test
    public void getName_should_return_username(){
       String username = user.getUsername();

        Assertions.assertEquals(username,user.getName());
    }

    @Test
    public void toString_Should_return_String(){
        Assertions.assertEquals(String.format(UserImpl.USER_TO_STRING,TestData.User.VALID_USERNAME,UserRole.DEVELOPER),user.toString());
    }

    @Test
    public void changePassword_Should_Change_User_Password(){
        String oldPass = user.getPassword();
        user.changePassword(oldPass+1);
        String newPass = user.getPassword();
        Assertions.assertNotEquals(oldPass, newPass);
    }


}
