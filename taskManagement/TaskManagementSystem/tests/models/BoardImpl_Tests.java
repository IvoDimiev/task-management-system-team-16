package models;

import core.Contracts.TaskManagementRepository;
import core.TaskManagementRepositoryImpl;
import exceptions.ElementNotFoundException;
import models.contracts.Board;
import models.contracts.HistoryLog;
import models.contracts.User;
import models.tasks.contracts.Assignable;
import models.tasks.contracts.Bug;
import models.tasks.contracts.Feedback;
import models.tasks.contracts.Story;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.TestData;

import java.util.List;

import static utils.Factory.*;

public class BoardImpl_Tests {
    private TaskManagementRepository taskManagementRepository;
    private User user;

    @BeforeEach
    public void before() {
        user = createNormalUser();
        taskManagementRepository = new TaskManagementRepositoryImpl();
        taskManagementRepository.addUser(user);
        taskManagementRepository.login(user);
    }

    @Test
    public void getHistory_should_returnCopy() {
        Board testBoard = createTestBoard();
        List<HistoryLog> logs = testBoard.getHistory();

        logs.add(new HistoryLogImpl("test log added"));

        Assertions.assertEquals(1, user.getHistory().size());
    }

    @Test
    public void getStories_should_returnCopy() {
        Board testBoard = createTestBoard();
        Story testStory = createNormalStory();
        List<Story> stories = testBoard.getStories();
        stories.add(testStory);

        Assertions.assertEquals(0, testBoard.getStories().size());
    }

    @Test
    public void getFeedbacks_should_returnCopy() {
        Board testBoard = createTestBoard();
        Feedback testFeedback = createNormalFeedback();
        List<Feedback> feedbacks = testBoard.getFeedback();
        feedbacks.add(testFeedback);

        Assertions.assertEquals(0, testBoard.getFeedback().size());
    }

    @Test
    public void getBugs_should_returnCopy() {
        Board testBoard = createTestBoard();
        Bug testBug = createNormalBug();
        List<Bug> bugs = testBoard.getBugs();
        bugs.add(testBug);

        Assertions.assertEquals(0, testBoard.getBugs().size());
    }

    @Test
    public void getAssignables_should_returnAssignables() {
        Board testBoard = createTestBoard();
        List<Assignable>assignables = testBoard.getAssignable();

        Assertions.assertFalse(assignables.stream().anyMatch(assignable -> assignable instanceof Feedback));
    }

    @Test
    public void findItemById_ShouldThrow_WhenItemDoesNotExist() {
        Board testBoard = new BoardImpl("TestBoard");
        Bug bug = createNormalBug();
        testBoard.addBug(bug);
        List<Bug>bugs = testBoard.getBugs();
        Assertions.assertThrows(ElementNotFoundException.class, () -> testBoard.findItemById(bugs,1141266,"Bug"));
    }

    @Test
    public void toString_Should_return_String(){
        Assertions.assertEquals(String.format(BoardImpl.BOARD_TO_STRING,TestData.Board.VALID_BOARD_NAME),createTestBoard().toString());
    }

    @Test
    public void addStory_shouldAddStoryToList() {
        Board board = createTestBoard();
        Story testStory = createNormalStory();
        int sizeBeforeAdd = board.getStories().size();
        board.addStory(testStory);

        Assertions.assertNotEquals(sizeBeforeAdd,board.getStories().size());
    }

    @Test
    public void addBug_shouldAddBugToList() {
        Board board = createTestBoard();
        Bug testBug = createNormalBug();
        int sizeBeforeAdd = board.getBugs().size();
        board.addBug(testBug);

        Assertions.assertNotEquals(sizeBeforeAdd,board.getBugs().size());
    }

    @Test
    public void addFeedback_shouldAddFeedbackToList() {
        Board board = createTestBoard();
        Feedback testFeedback = createNormalFeedback();
        int sizeBeforeAdd = board.getFeedback().size();
        board.addFeedback(testFeedback);

        Assertions.assertNotEquals(sizeBeforeAdd,board.getFeedback().size());
    }

}
