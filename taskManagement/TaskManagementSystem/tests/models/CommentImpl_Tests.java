package models;

import core.Contracts.TaskManagementRepository;
import core.TaskManagementRepositoryImpl;
import exceptions.InvalidUserInputException;
import models.contracts.Comment;
import models.contracts.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static utils.Factory.createNormalUser;
import static utils.TestUtilities.initializeStringWithSize;


public class CommentImpl_Tests {
    private TaskManagementRepository taskManagementRepository;
    private User user;

    @BeforeEach
    public void before() {
        user = createNormalUser();
        taskManagementRepository = new TaskManagementRepositoryImpl();
        taskManagementRepository.addUser(user);
        taskManagementRepository.login(user);
    }

    @Test
    public void CommentImpl_ShouldImplementCommentInterface() {
        CommentImpl comment = new CommentImpl(user, "test content");
        Assertions.assertTrue(comment instanceof Comment);
    }

    @ParameterizedTest(name = "with length {0}")
    @ValueSource(ints = {CommentImpl.CONTENT_MIN_VALUE - 1, CommentImpl.CONTENT_MAX_VALUE + 1})
    public void should_throwException_when_modelNameLengthOutOfBounds(int testLength) {
        // Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () ->
                new CommentImpl(user,
                        initializeStringWithSize(testLength)));
    }

    @Test
    public void Constructor_ShouldThrow_WhenContentDoesNotMatchValidLength() {
        //Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> new CommentImpl(user,
                        "content"));
    }

    @Test
    public void toString_Should_return_Correct_String_Format(){
       Comment testComment = new CommentImpl(user,"Manchester United");
        Assertions.assertEquals(String.format
                (CommentImpl.COMMENT_TO_STRING,testComment.getContent(),user),testComment.toString());
    }
}
