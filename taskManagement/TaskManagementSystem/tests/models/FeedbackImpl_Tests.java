package models;

import core.Contracts.TaskManagementRepository;
import core.TaskManagementRepositoryImpl;
import exceptions.InvalidUserInputException;
import models.contracts.User;
import models.tasks.contracts.Feedback;
import models.tasks.contracts.Statusable;
import models.tasks.enums.FeedbackStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.TestData;

import static utils.Factory.createNormalFeedback;
import static utils.Factory.createNormalUser;

public class FeedbackImpl_Tests {
    private TaskManagementRepository taskManagementRepository;
    private User user;

    @BeforeEach
    public void before() {
        user = createNormalUser();
        taskManagementRepository = new TaskManagementRepositoryImpl();
        taskManagementRepository.addUser(user);
        taskManagementRepository.login(user);
    }

    @Test
    public void changeRating_ShouldChange_Rating(){
        Feedback testFeedback = createNormalFeedback();
        int testFeedbackRatingBefore = testFeedback.getRating();
        testFeedback.changeRating(TestData.Feedback.VALID_RATING+1);

        Assertions.assertNotSame(testFeedbackRatingBefore, testFeedback.getRating());
    }

    @Test
    public void changeRating_ShouldThrow_When_RatingIsTheSame(){
        Feedback testFeedback = createNormalFeedback();

        Assertions.assertThrows(InvalidUserInputException.class, () -> testFeedback.changeRating(TestData.Feedback.VALID_RATING));
    }

    @Test
    public void changeStatus_ShouldThrow_When_StatusIsTheSame(){
        Feedback testFeedback = createNormalFeedback();

        Assertions.assertThrows(InvalidUserInputException.class, () -> testFeedback.changeStatus(TestData.Feedback.VALID_STATUS));
    }

    @Test
    public void changeStatus_ShouldChange_Status(){
        Feedback testFeedback = createNormalFeedback();
        Statusable testFeedbackStatusBefore = testFeedback.getStatus();
        testFeedback.changeStatus(FeedbackStatus.UNSCHEDULED);

        Assertions.assertNotSame(testFeedbackStatusBefore, testFeedback.getStatus());
    }
}
