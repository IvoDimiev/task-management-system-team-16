package models;

import core.Contracts.TaskManagementRepository;
import models.contracts.HistoryLog;
import models.contracts.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class HistoryLogImpl_Tests {
    private final String DESCRIPTION = "test description";
    private TaskManagementRepository taskManagementRepository;
    private User user;

    @BeforeEach
    public void before() {

    }

    //should throw when description is null

    @Test
    public void setDescription_should_createLog_when_descriptionIsNotNull() {
        HistoryLog log = new HistoryLogImpl(DESCRIPTION);

        Assertions.assertEquals(DESCRIPTION, log.getDescription());
    }

}
