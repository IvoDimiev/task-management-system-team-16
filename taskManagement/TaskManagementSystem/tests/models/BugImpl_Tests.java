package models;

import core.Contracts.TaskManagementRepository;
import core.TaskManagementRepositoryImpl;
import exceptions.InvalidUserInputException;
import models.contracts.User;
import models.tasks.BugImpl;
import models.tasks.contracts.Bug;
import models.tasks.contracts.Statusable;
import models.tasks.enums.BugStatus;
import models.tasks.enums.Priority;
import models.tasks.enums.Severity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.TestData;

import static utils.Factory.createNormalBug;
import static utils.Factory.createNormalUser;

public class BugImpl_Tests {
    private TaskManagementRepository taskManagementRepository;
    private User user;

    @BeforeEach
    public void before() {
        user = createNormalUser();
        taskManagementRepository = new TaskManagementRepositoryImpl();
        taskManagementRepository.addUser(user);
        taskManagementRepository.login(user);
    }
    @Test
    public void changeStatus_ShouldChange_Status(){
        Bug testBug = createNormalBug();
        Statusable testBugStatus = testBug.getStatus();
        testBug.changeStatus(BugStatus.FIXED);

        Assertions.assertNotSame(testBugStatus, testBug.getStatus());
    }

    @Test
    public void changeStatus_ShouldThrow_When_StatusIsTheSame(){
        Bug testBug = createNormalBug();

        Assertions.assertThrows(IllegalArgumentException.class, () -> testBug.changeStatus(TestData.Bug.VALID_STATUS));
    }
    @Test
    public void changePriority_ShouldChange_Priority(){
        Bug testBug = createNormalBug();
        Priority testBugPriorityBefore = testBug.getPriority();
        testBug.changePriority(Priority.HIGH);

        Assertions.assertNotSame(testBugPriorityBefore, testBug.getPriority());
    }
    @Test
    public void changePriority_ShouldThrow_When_PriorityIsTheSame(){
        Bug testBug = createNormalBug();

        Assertions.assertThrows(InvalidUserInputException.class, () -> testBug.changePriority(TestData.Bug.VALID_BUG_PRIORITY));
    }
    @Test
    public void changeSeverity_ShouldChange_Severity(){
        Bug testBug = createNormalBug();
        Severity testBugSeverity = testBug.getSeverity();
        testBug.changeSeverity(Severity.MAJOR);

        Assertions.assertNotSame(testBugSeverity, testBug.getSeverity());
    }

    @Test
    public void changeSeverity_ShouldThrow_When_SeverityIsTheSame(){
        Bug testBug = createNormalBug();

        Assertions.assertThrows(InvalidUserInputException.class, () -> testBug.changeSeverity(TestData.Bug.VALID_SEVERITY));
    }

    @Test
    public void putAssignee_ShouldPut_new_Assignee(){
        Bug testBug = createNormalBug();
        User testBugAssignee = testBug.getAssignee();
        testBug.putAssignee(user);

        Assertions.assertNotSame(testBugAssignee, testBug.getAssignee());
    }

    @Test
    public void putAssignee_ShouldThrow_When_AssigneeIsTheSame(){
        Bug testBug = createNormalBug();

        Assertions.assertThrows(IllegalArgumentException.class, () -> testBug.putAssignee(BugImpl.UNASSIGNED_USER));
    }

}
