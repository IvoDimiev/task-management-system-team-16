package models;

import core.Contracts.TaskManagementRepository;
import core.TaskManagementRepositoryImpl;
import exceptions.InvalidUserInputException;
import models.contracts.User;
import models.tasks.StoryImpl;
import models.tasks.contracts.Statusable;
import models.tasks.contracts.Story;
import models.tasks.enums.Priority;
import models.tasks.enums.Size;
import models.tasks.enums.StoryStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.TestData;

import static utils.Factory.createNormalStory;
import static utils.Factory.createNormalUser;

public class StoryImpl_Tests {

    private TaskManagementRepository taskManagementRepository;
    private User user;

    @BeforeEach
    public void before() {
        user = createNormalUser();
        taskManagementRepository = new TaskManagementRepositoryImpl();
        taskManagementRepository.addUser(user);
        taskManagementRepository.login(user);
    }

    @Test
    public void changePriority_ShouldChange_Priority(){
        Story testStory = createNormalStory();
        Priority testStoryPriorityBefore = testStory.getPriority();
        testStory.changePriority(Priority.HIGH);

        Assertions.assertNotSame(testStoryPriorityBefore, testStory.getPriority());
    }
    @Test
    public void changePriority_ShouldThrow_When_PriorityIsTheSame(){
        Story testStory = createNormalStory();

        Assertions.assertThrows(IllegalArgumentException.class, () -> testStory.changePriority(TestData.Story.VALID_STORY_PRIORITY));
    }

    @Test
    public void changeSize_ShouldChange_Size(){
        Story testStory = createNormalStory();
        Size testStorySizeBefore = testStory.getSize();
        testStory.changeSize(Size.LARGE);

        Assertions.assertNotSame(testStorySizeBefore, testStory.getSize());
    }

    @Test
    public void changeSize_ShouldThrow_When_SizeIsTheSame(){
        Story testStory = createNormalStory();

        Assertions.assertThrows(InvalidUserInputException.class, () -> testStory.changeSize(TestData.Story.VALID_SIZE));
    }

    @Test
    public void changeStatus_ShouldChange_Status(){
        Story testStory = createNormalStory();
        Statusable testStoryStatusBefore = testStory.getStatus();
        testStory.changeStatus(StoryStatus.DONE);

        Assertions.assertNotSame(testStoryStatusBefore, testStory.getStatus());
    }

    @Test
    public void changeStatus_ShouldThrow_When_StatusIsTheSame(){
        Story testStory = createNormalStory();

        Assertions.assertThrows(InvalidUserInputException.class, () -> testStory.changeStatus(TestData.Story.VALID_STATUS));
    }

    @Test
    public void putAssignee_ShouldPut_new_Assignee(){
        Story testStory = createNormalStory();
        User testStoryAssignee = testStory.getAssignee();
        testStory.putAssignee(user);

        Assertions.assertNotSame(testStoryAssignee, testStory.getAssignee());
    }

    @Test
    public void putAssignee_ShouldThrow_When_AssigneeIsTheSame(){
        Story testStory = createNormalStory();

        Assertions.assertThrows(InvalidUserInputException.class, () -> testStory.putAssignee(StoryImpl.UNASSIGNED_USER));
    }
}
