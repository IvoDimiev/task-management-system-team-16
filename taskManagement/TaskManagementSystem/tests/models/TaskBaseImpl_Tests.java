package models;

import core.Contracts.TaskManagementRepository;
import core.TaskManagementRepositoryImpl;
import models.contracts.Comment;
import models.contracts.User;
import models.tasks.contracts.Bug;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static utils.Factory.createNormalBug;
import static utils.Factory.createNormalUser;

public class TaskBaseImpl_Tests {

    private TaskManagementRepository taskManagementRepository;
    private User user;

    @BeforeEach
    public void before() {
        user = createNormalUser();
        taskManagementRepository = new TaskManagementRepositoryImpl();
        taskManagementRepository.addUser(user);
        taskManagementRepository.login(user);
    }

    @Test
    public void getComments_should_returnCopy() {
        Comment testComment = new CommentImpl(user,"test content ");
        Bug bug = createNormalBug();
        List<Comment> comments = bug.getComments();
        comments.add(testComment);


        Assertions.assertEquals(0, bug.getComments().size());
    }
}