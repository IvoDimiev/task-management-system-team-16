<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

#Project Description

---
##OOP Teamwork
###`Ivo Dimiev & Sofi Stoyanova - Team 16` 

---
##Task Management System

Design and implement a **Tasks Management** console application.
The application will be used by a small team of developers, who need to keep track of all the tasks, surrounding a software product they are building.
 <br /> 
---
##models hierarchy
<p><img src="https://i.ibb.co/ykqctCt/Hierarchy-TASKMANAGEMENTx1.png" alt="SCHEME" border="0" ><p>

---
##Operations

The application supports the following operations:

NOTE: Use **_{{_** at the start and close with **_}}_** if your string parameter contains empty space " " (e.g. for a title with empty space {{**__This is an example__**}} ). 

---
###addOperations
- `AddCommentToATask [teamname] [boardname] [taskId] [content]` - Add a comment to a specific task. <br />(e.g. AddCommentToATask project review 1 {{This was hard!}} )
---
###assignOperations
- `AssignTaskToUser [teamname] [boardname] [taskId] [assignee username]`- Assigns a specific task to a specific user. <br />(e.g. AssignTaskToUser project review 1 developer)
- `UnassignTaskToUser [teamname] [boardname] [taskId] [assignee username]`- Removes an assignee from a specific task, and the task gets assignee "Unassigned".<br />(e.g. UnassignTaskToUser project review 1 )
---
###changeOperations
-  `ChangeBugPriority [teamname] [boardname] [taskId] [priority]`- Change the priority of a specific bug. <br /> (e.g. ChangeBugPriority project review 1 High )
-  `ChangeBugSeverity [teamname] [boardname] [taskId] [severity]`- Change the severity of a specific bug. <br />(e.g. ChangeBugSeverity project review 1 Critical )
-  `ChangeBugStatus [teamname] [boardname] [taskId] [status]`- Change the status of a specific bug. <br />(e.g. ChangeBugStatus project review 1 Active )
-  `ChangeFeedbackRating [teamname] [boardname] [taskId] [rating]`- Change the rating of a specific feedback. <br />(e.g. ChangeFeedbackRating project review 1 40 )
-  `ChangeFeedbackStatus[teamname] [boardname] [taskId] [status]`- Change the priority of a specific feedback. <br />(e.g. ChangeFeedbackStatus project review 1 New )
-  `ChangeStoryPriority [teamname] [boardname] [taskId] [priority]`- Change the priority of a specific story. <br />(e.g. ChangeStoryPriority project review 1 High )
-  `ChangeStorySize [teamname] [boardname] [taskId] [size]`- Change the size of a specific story. <br />(e.g. ChangeStorySize project review 1 Large )
-  `ChangeStoryStatus [teamname] [boardname] [taskId] [status]`- Change the Status of a specific story. <br />(e.g. ChangeStoryStatus project review 1 Not_Done )
---
###createOperations
- `CreateBoard [boardname] [teamname]`- Create a new board in a specific team. <br /> (e.g. CreateBoard review project )
- `CreateBug [teamname] [boardname] [bugTitle] [bugDescription] [priority] [severity] [stepsToReproduce]`- Create a bug in a specific board and team. <br /> (e.g. CreateBug project review {{The program freezes when the Log In button.}} {{This needs to be fixed quickly!}} High Critical {{"1. Open the application; 2. Click "Log In"; 3. The application freezes!"}})
- `CreateFeedback [boardname] [teamname] [feedbackTitle] [feedbackDescription] [rating]`- Create a feedback in a specific board and team. <br /> (e.g. CreateFeedback project review {{This is my feedback}} {{Our project is the best}} 22)
- `CreateStory [boardname] [teamname] [storyTitle] [storyDescription] [priority] [size]`- Create a story in a specific board and team. <br /> (e.g. CreateStory project review {{We are going to have great holidays}} {{ i am going to fix this project even on the first of January}} Medium Small)
- `CreateTeam [teamname]`- Create a new team. <br /> (e.g. CreateTeam review)
---
###listingOperations
- `FilterAllByTitle [teamname] [boardname] [filterWord]`- Filters all tasks from a specific board and team, by a specific word from the title. <br /> (e.g. FilterAllByTitle project review money)
- `FilterBugBySeverity [teamname] [boardname] [severity]`- Filters all bugs from a specific board and team, by a specific severity. <br /> (e.g. FilterBugBySeverity project review minor)
- `FilterBugByStatusAndAssignee [teamname] [boardname] [status] [assigneeUsername]`- Filters all bugs from a specific board and team, by a specific status and assignee. <br /> (e.g. FilterBugByStatusAndAssignee project review Active Ivo)
- `FilterBugByStatus [teamname] [boardname] [status]`- Filters all bugs from a specific board and team, by a specific status. <br /> (e.g. FilterBugByStatus project review Fixed)
- `FilterByAssignee [teamname] [boardname] [assigneeUsername]`- Filters all tasks from a specific board and team, by a specific assignee. <br /> (e.g. FilterByAssignee project review Valina)
- `FilterFeedbackByStatus [teamname] [boardname] [status]`- Filters all feedback from a specific board and team, by a specific status. <br /> (e.g. FilterFeedbackByStatus project review New)
- `FilterStoryByStatusAndAssignee [teamname] [boardname] [status] [assigneeUsername]`- Filters all stories from a specific board and team, by a specific status and assignee. <br /> (e.g. FilterStoryByStatusAndAssignee project review Done Ivo)
- `FilterStoryByStatus [teamname] [boardname] [status] `- Filters all stories from a specific board and team, by a specific status. <br /> (e.g. FilterStoryByStatus project review Active Ivo)
- `ListAll [teamname] [boardname]`- Lists all tasks from a specific board and team. <br /> (e.g. ListAll project review )
- `ListBugs [teamname] [boardname]`- Lists all bugs from a specific board and team. <br /> (e.g. ListBugs project review )
- `ListFeedbacks [teamname] [boardname]`- Lists all feedbacks from a specific board and team. <br /> (e.g. ListFeedbacks project review )
- `ListStories [teamname] [boardname]`- Lists all stories from a specific board and team. <br /> (e.g. ListStories project review )
- `SortAllByTitle [teamname] [boardname]`- Sort all tasks from a specific board and team by title. <br /> (e.g. SortAllByTitle project review )
- `SortBugBySeverity [teamname] [boardname] `- Sort all bugs from a specific board and team by severity. <br /> (e.g. SortBugsBySeverity project review )
- `SortBugByTitle [teamname] [boardname] `- Sort all bugs from a specific board and team by title. <br /> (e.g. SortBugsByTitle project review )
- `SortByPriority [teamname] [boardname] `- Sort all prioritisables from a specific board and team by priority. <br /> (e.g. SortByPriority project review )
- `SortFeedbackByRating [teamname] [boardname] `- Sort all feedbacks from a specific board and team by rating. <br /> (e.g. SortFeedbackByRating project review )
- `SortFeedbackByTitle [teamname] [boardname] `- Sort all feedbacks from a specific board and team by title. <br /> (e.g. SortFeedbackByTitle project review )
- `SortStoryBySize [teamname] [boardname] `- Sort all stories from a specific board and team by size. <br /> (e.g. SortStoryBySize project review )
- `SortStoryByTitle [teamname] [boardname] `- Sort all bugs from a specific board and team by Title. <br /> (e.g. SortStoryByTitle project review )
---
###showOperations
- `ShowAllTeamBoards [teamname]`- Show all the boards that a specific team has. <br /> (e.g. ShowAllTeamBoards project)
- `ShowAllTeamMembers [teamname]`- Show all the members that a specific team has. <br /> (e.g. ShowAllTeamMembers project)
- `ShowAllTeams [teamname]`- Show all the teams. <br /> (e.g. ShowAllTeams)
- `ShowAllUsers[teamname]`- Show all the users. <br /> (e.g. ShowAllUsers)
- `ShowBoardSActivity [teamname]`- Show all logs that the board has. <br /> (e.g. ShowBoardSActivity project review)
- `ShowTeamSActivity [teamname]`- Show all logs that the user has. <br /> (e.g. ShowTeamSActivity project)
- `ShowUserSActivity [teamname]`- Show all the logs that the user has. <br /> (e.g. ShowUserSActivity developer)
- `ShowTaskSActivity [teamname] [boardname] [id]`- Show all the logs that the task has. <br /> (e.g. ShowTaskSActivity project review 1)
---
###userOperations
- `ChangePassword [oldPassword] [newPassword]`- Changed the password of the logged user. <br /> (e.g. ChangePassword 123456 123456789)
- `InviteUser [teamname] [username] [password] [userRole] `- Invite a user to a team, by adding him to the team with base username and password, which he can change later. <br /> (e.g. InviteUser project ivo 123123 developer)
- `login [username] [password]`- log into the console application. <br /> (e.g. login ivo 123123)
- `logout`- log out of the application. <br /> (e.g. logout)
- `RegisterNewUser [username] [passowrd] [userRole]`- Register a new user, without logging him in. <br /> (e.g. RegisterNewUser ivo 123123 developer )
---


##Use cases


###Use case #1

RegisterNewUser developer 12345 <br />
CreateTeam project<br />
CreateBoard review project<br />
CreateBug project review {{The program freezes when the Log In button.}} {{This needs to be fixed quickly!}} High Critical {{"1. Open the application; 2. Click "Log In"; 3. The application freezes!"}}


###Use case #2

RegisterNewUser developer 12345 <br />
CreateTeam project <br />
CreateBoard review project <br />
CreateBug project review {{The program freezes when the Log In button.}} {{This needs to be fixed quickly!}} High Critical {{"1. Open the application; 2. Click "Log In"; 3. The application freezes!"}} <br />
CreateBug project review {{The program restarts when the exit is clicked}} {{This needs to be fixed quickly!}} High Critical {{"1. Open the application; 2. Click "exit"; 3. The application freezes!"}} <br />
CreateBug project review {{The program freezes when the run button is clicked}} {{This needs to be fixed quickly!}} High Critical {{"1. Open the application; 2. Click "run"; 3. The application freezes!"}} <br />
InviteUser project pesho 123456 <br />
FilterBugBySeverity project review Critical <br /> 
AssignTaskToUser project review 1 pesho <br /> 
AssignTaskToUser project review 2 pesho <br />
AssignTaskToUser project review 3 pesho <br />


###Use case #3

RegisterNewUser developer 12345 <br />
CreateTeam project <br />
CreateBoard review project <br />
CreateBug project review {{The program freezes when the Log In button.}} {{This needs to be fixed quickly!}} High Critical {{"1. Open the application; 2. Click "Log In"; 3. The application freezes!"}} <br />
AddCommentToATask project review 1 {{This one took me a while, but it is fixed now!}} <br />
ChangeBugStatus project review 1 Fixed <br />
ShowTaskActivity project review 1 <br />





